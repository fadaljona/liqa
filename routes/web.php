<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('rooms_direction/{location}/directions', 'RoomController@directions')->name('rooms.direction');

Route::get('/lang/{local}', 'HomeController@changeLocal')->name('lang');

Route::post('rooms/{room}/reservations', 'RoomController@reservations')->name('rooms.reservations');

Route::group(['middleware' => ['language']], function () {
    Auth::routes();

    Route::get('/authorize', 'AuthController@index');

    Route::get('/i/{code}', 'VisitController@visitCode')->name('visit.code');

    Route::get('/rates/{code}', 'VisitController@rateUs')->name('visit.rates');
    Route::post('/rates/{code}', 'VisitController@storeRate')->name('visits.storeRate');

    Route::get('rooms/{id}/visit', 'RoomController@visit')->name('rooms.visit');
    Route::group(['middleware' => ['auth', 'language', 'permission']], function () {

        Route::get('/', 'HomeController@index')->name('home');

        Route::get('/visits', 'VisitController@index')->name('visits.index');
        Route::get('/visits/create', 'VisitController@create')->name('visits.create');
        Route::post('/visits/store', 'VisitController@store')->name('visits.store');
        Route::get('/visits/{visitor_id}/check_in', 'VisitController@checkIn')->name('visits.checkIn');
        Route::get('/visits/{visitor_id}/check_out', 'VisitController@checkOut')->name('visits.checkOut');
        Route::get('/visits/init', 'VisitController@init')->name('visits.init');
        Route::get('/visits/init/{id}/accepted', 'VisitController@accepted')->name('visits.accepted');
        Route::post('/visits/init/{id}/decline', 'VisitController@decline')->name('visits.decline');
        Route::delete('/visits/{visit}','VisitController@cancel')->name('visits.cancel');
        Route::get('/visits/{visit}/notes', 'VisitController@createNote')->name('visits.createNote');
        Route::put('/visits/{visit}/notes', 'VisitController@storeNote')->name('visits.storeNote');




        Route::resource('rooms', 'RoomController');

        Route::get('setting', 'SettingController@edit')->name('setting.edit');
        Route::put('setting', 'SettingController@update')->name('setting.update');
        Route::get('setting/{id}','SettingController@delete')->name('setting.logo.delete');

        Route::resource('users', 'UserController')->except('show');

//        Route::resource('departments', 'DepartmentController')->except('show');

        Route::get('/reports', 'ReportController@index')->name('reports.index');
        Route::get('/reports/meetings/export', 'ReportController@meetingsExport')->name('reports.meetings.export');
        Route::get('/reports/visitors/export', 'ReportController@visitorsExport')->name('reports.visitors.export');
        Route::get('/reports/users/export', 'ReportController@usersExport')->name('reports.users.export');
        Route::get('/reports/rates/export', 'ReportController@ratesExport')->name('reports.rates.export');


    });

});