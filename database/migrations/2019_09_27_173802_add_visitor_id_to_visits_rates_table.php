<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVisitorIdToVisitsRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visits_rates', function (Blueprint $table) {
            $table->integer('visitor_id')->after('visit_id')->nullable()->unsigned();
            $table->foreign('visitor_id')->references('id')->on('visitors')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visits_rates', function (Blueprint $table) {
            $table->dropColumn('visitor_id');
            $table->dropForeign(['visitor_id']);
        });
    }
}
