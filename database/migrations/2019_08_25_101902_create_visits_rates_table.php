<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits_rates', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('cleanness')->unsigned();
            $table->integer('host')->unsigned();
            $table->integer('service')->unsigned();

            $table->text('text')->nullable();

            $table->integer('visit_id')->unsigned();
            $table->foreign('visit_id')->references('id')->on('visits')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits_rates');
    }
}
