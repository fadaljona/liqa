<?php

use App\Visitor;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSmsStateToVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visitors', function (Blueprint $table) {
            $table->enum('sms_state', ['init', 'send', 'error'])->default('init');
            $table->enum('email_state', ['init', 'send', 'error'])->default('init');
        });

      $visitors =  Visitor::all();

      foreach ($visitors as $visitor)
      {

          $visitor->update([
              'sms_state' => $visitor->status,
              'email_state' => $visitor->status,

          ]);

      }

        Schema::table('visitors', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visitors', function (Blueprint $table) {
            $table->enum('status', ['init', 'send', 'error'])->default('init');
        });

        $visitors =  Visitor::all();

        foreach ($visitors as $visitor)
        {
            $visitor->update([
                'status' => $visitor->sms_state
            ]);
        }

        Schema::table('visitors', function (Blueprint $table) {
            $table->dropColumn('sms_state');
            $table->dropColumn('email_state');
        });
    }
}
