<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitors', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('mobile');
            $table->string('email')->nullable();

            $table->string('parking')->nullable();
            $table->string('code');

            $table->boolean('check_in')->default(0);
            $table->boolean('check_out')->default(0);

            $table->integer('visit_id')->unsigned();
            $table->foreign('visit_id')->references('id')->on('visits')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->enum('status', ['init', 'send', 'error'])->default('init');
            $table->string('lang');
            $table->boolean('view_code')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitors');
    }
}