<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Visitor extends Model
{
    use Notifiable;

    protected $table = 'visitors';

    protected $fillable = [
        'visit_id',
        'name',
        'mobile',
        'email',
        'parking',
        'code',
        'check_in',
        'check_out',
        'send_sms',
        'lang',
        'status', // removed
        'sms_state',
        'email_state',
        'view_code',
        'has_rated',
    ];


    public function visit()
    {
        return $this->belongsTo(Visit::class, 'visit_id');
    }
}