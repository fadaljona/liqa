<?php

namespace App\Listeners;

use App\Events\MeetingRejected;
use App\Notifications\UserMeetingRejectionNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MeetingRejectedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MeetingRejected  $event
     * @return void
     */
    public function handle(MeetingRejected $event)
    {
        $event->meeting->user->notify(new UserMeetingRejectionNotification($event->meeting, $event->reason));
    }
}
