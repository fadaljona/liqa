<?php

namespace App\Listeners;

use App\Events\MeetingAccepted;
use App\Notifications\VisitorEmailInvitationNotification;

class InviteVisitorsViaEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MeetingAccepted  $event
     * @return void
     */
    public function handle(MeetingAccepted $event)
    {
        if(count($event->meeting->visitor) > 0){
            foreach($event->meeting->visitor as $guest){
                $guest->notify((new VisitorEmailInvitationNotification($event->meeting, $guest)));
            }
        }
    }
}
