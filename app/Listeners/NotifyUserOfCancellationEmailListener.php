<?php

namespace App\Listeners;

use App\Events\MeetingCancelled;
use App\Notifications\UserCancellationEmailNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyUserOfCancellationEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MeetingCancelled  $event
     * @return void
     */
    public function handle(MeetingCancelled $event)
    {
        $event->meeting->user->notify((new UserCancellationEmailNotification($event->meeting)));
    }
}
