<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\RateUsSmsNotification;

class SendVisitorsRatesViaSmsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if(count($event->visit->visitor) > 0){
            foreach($event->visit->visitor as $guest){
                if($guest->check_in && $guest->check_out) {
                    $guest->notify((new RateUsSmsNotification($event->visit)));
                }
            }
        }
    }
}
