<?php

namespace App\Listeners;

use App\Events\MeetingAccepted;
use App\Notifications\MeetingAcceptanceNotification;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyUserOfAcceptanceViaEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MeetingAccepted  $event
     * @return void
     */
    public function handle(MeetingAccepted $event)
    {
        $event->meeting->user->notify((new MeetingAcceptanceNotification($event->meeting)));
    }
}
