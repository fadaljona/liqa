<?php

namespace App\Listeners;

use App\Events\MeetingCancelled;
use App\Notifications\VisitorCancellationEmailNotification;

class NotifyVisitorsOfCancellationViaEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MeetingCancelled  $event
     * @return void
     */
    public function handle(MeetingCancelled $event)
    {
        if(count($event->meeting->visitor) > 0){
            foreach($event->meeting->visitor as $guest){
                $guest->notify((new VisitorCancellationEmailNotification($event->meeting, $guest)));
            }
        }
    }
}
