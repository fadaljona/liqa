<?php

namespace App\Listeners;

use App\Events\CheckedInVisitor;
use App\Notifications\CheckInVisitorNotification;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendHostAboutCheckedInListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MeetingAccepted  $event
     * @return void
     */
    public function handle(CheckedInVisitor $event)
    {
        $event->host->notify((new CheckInVisitorNotification($event)));
    }
}
