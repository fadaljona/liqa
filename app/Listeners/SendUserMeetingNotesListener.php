<?php

namespace App\Listeners;

use App\Notifications\MeetingNotesEmailNotification;
use App\Events\NotesSaved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class SendUserMeetingNotesListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotesSaved  $event
     * @return void
     */
    public function handle(NotesSaved $event)
    {
        $event->visit->user->notify((new MeetingNotesEmailNotification($event)));
        Notification::send($event->visitors,(new MeetingNotesEmailNotification($event)));
    }
}
