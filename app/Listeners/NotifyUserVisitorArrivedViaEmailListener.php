<?php

namespace App\Listeners;

use App\Events\CheckedInVisitor;
use App\Notifications\CheckInVisitorNotification;

class NotifyUserVisitorArrivedViaEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CheckedInVisitor  $event
     */
    public function handle(CheckedInVisitor $event)
    {
        $event->host->notify((new CheckInVisitorNotification($event)));
    }
}
