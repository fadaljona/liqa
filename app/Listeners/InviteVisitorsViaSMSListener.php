<?php

namespace App\Listeners;

use App\Events\MeetingAccepted;
use App\Notifications\VisitorSMSInvitationNotification;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class InviteVisitorsViaSMSListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MeetingAccepted  $event
     * @return void
     */
    public function handle(MeetingAccepted $event)
    {
        if(count($event->meeting->visitor) > 0){
            foreach($event->meeting->visitor as $guest){
                $guest->notify((new VisitorSMSInvitationNotification($event->meeting)));
            }
        }
    }
}
