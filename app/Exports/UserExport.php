<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;

class UserExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data = collect([
            [
                'id',
                'name',
                'email',
                'language',
                'role',
                'type',
                'created_at',
            ],
        ]);

        User::get()
            ->map(function (User $user) use (&$data) {
                $data->push([
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'language' => $user->language,
                    'role' => $user->role,
                    'type' => $user->type,
                    'created_at' => $user->created_at,
                ]);
            });

        return $data;
    }
}
