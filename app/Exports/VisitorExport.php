<?php

namespace App\Exports;

use App\Visitor;
use Maatwebsite\Excel\Concerns\FromCollection;

class VisitorExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data = collect([
            [
                'id',
                'name',
                'mobile',
                'email',
                'parking',
                'check_in',
                'check_out',
                'visit',
                'lang',
                'created_at',
            ],
        ]);

        Visitor::with('visit')
            ->get()
            ->map(function (Visitor $visitor) use (&$data) {
                $data->push([
                    'id' => $visitor->id,
                    'name' => $visitor->name,
                    'mobile' => $visitor->mobile,
                    'email' => $visitor->email,
                    'parking' => $visitor->parking,
                    'check_in' => $visitor->check_in,
                    'check_out' => $visitor->check_out,
                    'visit' => $visitor->visit->title,
                    'lang' => $visitor->lang,
                    'created_at' => $visitor->created_at,
                ]);
            });

        return $data;
    }
}
