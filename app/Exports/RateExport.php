<?php

namespace App\Exports;

use App\VisitsRate;
use Maatwebsite\Excel\Concerns\FromCollection;

class RateExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data = collect([
            [
                'id',
                'cleanness',
                'host',
                'service',
                'text',
                'visit',
                'created_at',
            ],
        ]);

        VisitsRate::with('visit')
            ->get()
            ->map(function (VisitsRate $visit_rate) use (&$data) {
                $data->push([
                    'id' => $visit_rate->id,
                    'cleanness' => $visit_rate->cleanness,
                    'host' => $visit_rate->host,
                    'service' => $visit_rate->service,
                    'text' => $visit_rate->text,
                    'visit' => $visit_rate->visit->title,
                    'created_at' => $visit_rate->created_at,
                ]);
            });

        return $data;
    }
}
