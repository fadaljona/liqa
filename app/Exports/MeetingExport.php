<?php

namespace App\Exports;

use App\Visit;
use Maatwebsite\Excel\Concerns\FromCollection;

class MeetingExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data = collect([
            [
                'id',
                'title',
                'date',
                'time_start',
                'time_end',
                'needs',
                'room_ar',
                'room_en',
                'user',
                'status',
                'priority',
                'agenda',
                'notes',
                'created_at',
            ],
        ]);

        Visit::with('user', 'room')
            ->get()
            ->map(function (Visit $visit) use (&$data) {
                $data->push([
                    'ID' => $visit->id,
                    'title' => $visit->title,
                    'date' => $visit->date,
                    'time_start' => $visit->time_start,
                    'time_end' => $visit->time_end,
                    'needs' => $visit->needs,
                    'room_ar' => $visit->room->name_ar,
                    'room_en' => $visit->room->name_en,
                    'user' => $visit->user->name,
                    'status' => $visit->status,
                    'priority' => $visit->priority,
                    'agenda' => $visit->agenda,
                    'notes' => $visit->notes,
                    'state' => $visit->state,
                    'created_at' => $visit->created_at,
                ]);
            });

        return $data;
    }
}
