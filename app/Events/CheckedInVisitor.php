<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;


class CheckedInVisitor
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public $visitor;
    public $host;

    public function __construct($host, $visitor)
    {
        $this->visitor = $visitor;
        $this->host = $host;
    }
}
