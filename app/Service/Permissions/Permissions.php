<?php

namespace App\Service\Permissions;

class Permissions
{
    const user = [
        "home",
        "lang",

        'visits.index',
        'visits.create',
        'visits.store',
        'visits.cancel',

        'logout'
    ];
    const reception = [
        "home",
        "lang",

        'visits.index',
        'visits.create',
        'visits.store',
        'visits.checkIn',
        'visits.checkOut',
        'visits.cancel',

        "logout",
    ];
    const admin = [
        "home",
        "lang",

        'visits.index',
        'visits.create',
        'visits.store',
        'visits.checkIn',
        'visits.checkOut',
        'visits.init',
        'visits.accepted',
        'visits.decline',
        'visits.cancel',
        'visits.createNote',
        'visits.storeNote',

        'rooms.index',
        "rooms.store",
        'rooms.edit',
        'rooms.update',
        'rooms.create',
        'rooms.destroy',

        "users.index",
        "users.create",
        "users.store",
        "users.edit",
        "users.update",
        "users.destroy",

        "reports.index",
        "reports.meetings.export",
        "reports.visitors.export",
        "reports.users.export",
        "reports.rates.export",

        "setting.edit",
        "setting.update",
        "setting.logo.delete",

        "logout",
    ];

    public static function candDO($level, $route)
    {
        $result = [];
        switch ($level) {
            case 'user':
                $result = self::user;
                break;
            case 'reception':
                $result = self::reception;
                break;
            case 'admin':
                $result = self::admin;
                break;
        }

        if (in_array($route, $result)) {
            return true;
        }

        return false;
    }
}