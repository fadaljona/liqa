<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Visit extends Model
{
    use SoftDeletes;

    protected $table = 'visits';

    protected $fillable = [
        'title',
        'date',
        'time_start',
        'time_end',
        'needs',
        'room_id',
        'user_id',
        'status',
        'priority',
	    'agenda',
	    'notes',
    ];


    protected $dates = [
        'date', 'created_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function room()
    {
        return $this->belongsTo(Room::class, 'room_id');
    }

    public function visitor()
    {
        return $this->hasMany(Visitor::class, 'visit_id');
    }

    public function cancellable()
    {
        $meeting_time = Carbon::createFromFormat('Y-m-d H:i:s', "". $this->date->format('Y-m-d') . " " . $this->time_start);

        if ($meeting_time->isPast()) {
            return false;
        }

        return true;
    }

    public function isPast()
    {
        $meeting_time = Carbon::createFromFormat('Y-m-d H:i:s', "". $this->date->format('Y-m-d') . " " . $this->time_start);

        if ($meeting_time->isPast()) {
            return true;
        }

        return false;
    }
}
