<?php

namespace App\Channels;

use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;


class SmsChannel
{
    /**
     * Send the given notification.
     *
     * @param mixed $notifiable
     * @param \Illuminate\Notifications\Notification $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toSMS($notifiable);

        $jsonObj = array(
            'sender'          => env('SMS_TAGNAME', 'liqa.io'),
            'mobile'          => env('SMS_USERNAME'),
            'password'        => env('SMS_PASSWORD'),
            'numbers'         => $notifiable->mobile,
            'msg'             => $message,
            'msgId'           => rand(1, 999999),
            'timeSend'        => '0',
            'dateSend'        => '0',
            'deleteKey'       => '55348',
            'lang'            => '3',
            'returnJson'      => '1',
            'notRepeat'       => '1',
            'applicationType' => 68,

        );

        $url = "http://www.mobily.ws/api/msgSend.php";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsonObj));
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        if (isset($result['ResponseStatus']) && $result['ResponseStatus'] == 'success') {
            $notifiable->update([
                'sms_state' => 'send',
            ]);
        } else {
            if (isset($result) && is_array($result)) {
                log::error('SMS ERROR : ', $result);
            } else {
                log::error('sms error : '.$result);
            }
            $notifiable->update([
                'sms_state' => 'error',
            ]);
        }
    }
}
