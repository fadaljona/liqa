<?php

namespace App\Notifications;

use App\Channels\SmsChannel;
use App\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class VisitorSMSInvitationNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public $meeting;


    public function __construct($accepted_meeting)
    {
        $this->meeting = $accepted_meeting;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toSms($notifiable)
    {
        $setting = Setting::latest()->first();

        if ($notifiable->lang == "en") {
            $message = "hello " . $notifiable->name
                . ",\nThis is your visit details to "
                . $setting->company_name_en . " With "
                . $this->meeting->user->name
                . "\nYour visit pass: " . route('visit.code', ['code' => $notifiable->code]);
        } else {
            $message = "عزيزي " . $notifiable->name
                . ",\nهذه تفاصيل زيارتك لـ"
                . $setting->company_name . "مع"
                . $this->meeting->user->name
                . "\n تصريح الزيارة'" . route('visit.code', ['code' => $notifiable->code]);
        }

        return $message;
    }
}
