<?php

namespace App\Notifications;

use App\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class VisitorCancellationEmailNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */


    public $meeting;

    public $guest;

    public function __construct($cancelled_meeting, $guest)
    {
        $this->meeting = $cancelled_meeting;
        $this->guest = $guest;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $setting = Setting::latest()->first();

        if ($notifiable->lang == "ar") {
            $subject = "لقد تم الغاء اجتماعكم بعنوان " . $this->meeting->title;
        } else {
            $subject = "Your meeting titled" . $this->meeting->title . " has been cancelled.";
        }
        return (new MailMessage)
            ->subject($subject)
            ->view('emails.visitor_cancellation_email_notification', [
                'notifiable' => $notifiable,
                'meeting' => $this->meeting,
                'company_name_ar' => $setting->company_name,
                'company_name_en' => $setting->company_name_en
            ]);
    }

    public function failed()
    {
        $this->guest->update([
            'email_state' => 'error'
        ]);
    }
}
