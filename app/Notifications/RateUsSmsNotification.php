<?php

namespace App\Notifications;

use App\Channels\SmsChannel;
use App\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class RateUsSmsNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public $visit;


    public function __construct($visit)
    {
        $this->visit = $visit;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toSms($notifiable)
    {
        if ($notifiable->lang == "en") {
            $message = "Thank you for visiting us and we are pleased to rate"
                       ." your visit to our company via the following link:"
                       .route('visit.rates', ['code' => $notifiable->code]);
        } else {
            $message = "شكرا لك  لزيرتنا ويسرنا تقيمك لتحسين الأداء"
                ."عبر الرابط التالي : "
                .route('visit.rates', ['code' => $notifiable->code]);
        }

        return $message;
    }
}
