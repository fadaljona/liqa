<?php

namespace App\Notifications;

use App\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserCancellationEmailNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public $meeting;


    public function __construct($cancelled_meeting)
    {
        $this->meeting = $cancelled_meeting;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $setting = Setting::latest()->first();

        if ($notifiable->language == "ar") {
            $subject = "تم الغاء اجتماعكم بعنوان " . $this->meeting->title;
        } else {
            $subject = "Your meeting titled " . $this->meeting->title . "has been cancelled successfully";
        }

        return (new MailMessage)
            ->subject($subject)
            ->view('emails.user_cancellation_email_notification', [
                'notifiable'      => $notifiable,
                'meeting'         => $this->meeting,
                'company_name_ar' => $setting->company_name,
                'company_name_en' => $setting->company_name_en
            ]);
    }
}
