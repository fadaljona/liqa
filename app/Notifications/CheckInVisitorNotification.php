<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CheckInVisitorNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public $visitor;

    public $host;

    public function __construct($event)
    {
        $this->host = $event->host;
        $this->visitor = $event->visitor;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable){
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($notifiable->language == 'ar') {
            $title = ' الضيف ' . $this->visitor->name. ' لقد وصل ' ;
        }else {
            $title = ' has arrived ' . $this->visitor->name. ' The guest ' ;
        }

        return (new MailMessage)
            ->subject($title)
            ->view('emails.check_in', [
                'title' => $title
            ]);
    }
}
