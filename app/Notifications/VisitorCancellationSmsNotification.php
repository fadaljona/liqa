<?php

namespace App\Notifications;

use App\Channels\SmsChannel;
use App\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class VisitorCancellationSmsNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public $meeting;


    public function __construct($cancelled_meeting)
    {
        $this->meeting = $cancelled_meeting;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toSms($notifiable)
    {

        $setting = Setting::latest()->first();

        if ($notifiable->lang == "ar") {

            $message = "عزيزي " . $notifiable->name
                . "،\nلقد تم الغاء اجتماعكم بعنوان "
                . $this->meeting->title
                . " مع " . $this->meeting->user->name
                . "\nمع اطيب التحيات"
                . "\n" . $setting->company_name;
        } else {

            $message = "Dear " . $notifiable->name
                . ",\nYour meeting titled " . $this->meeting->title
                . " With " . $this->meeting->user->name
                . "\nhas been cancelled."
                . "\nBest Regards,"
                . "\n" . $setting->company_name_en;
        }


        return $message;
    }
}
