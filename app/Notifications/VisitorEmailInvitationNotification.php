<?php

namespace App\Notifications;

use App\Service\ICS;
use App\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class VisitorEmailInvitationNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public $meeting;

    public $guest;


    public function __construct($accepted_meeting, $guest)
    {
        $this->meeting = $accepted_meeting;
        $this->guest = $guest;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $setting = Setting::latest()->first();

        if ($notifiable->lang == "ar") {
            $subject = "تفاصيل الزيارة لـ " . $setting->company_name;
        } else {
            $subject = "Visit details to " . $setting->company_name_en;
        }

        return (new MailMessage)
            ->subject($subject)
            ->attachData( 'BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//Microsoft Corporation//Outlook 11.0 MIMEDIR//EN
METHOD:REQUEST
CALSCALE:GREGORIAN
BEGIN:VTIMEZONE
TZID:Asia/Qatar
BEGIN:STANDARD
DTSTART:'.$this->meeting->date->setTimeFromTimeString($this->meeting->time_start)->format('Ymd\THis').'
TZOFFSETFROM:+0300
TZOFFSETTO:+0300
TZNAME:CST
END:STANDARD
END:VTIMEZONE
BEGIN:VEVENT
ORGANIZER:MAILTO:'.$this->meeting->user->email.'
DTSTART;TZID=Asia/Qatar:'.$this->meeting->date->setTimeFromTimeString($this->meeting->time_start)->format('Ymd\THis').'
DTEND;TZID=Asia/Qatar:'.$this->meeting->date->setTimeFromTimeString($this->meeting->time_end)->format('Ymd\THis').'
TRANSP:OPAQUE
SEQUENCE:0
UID:'.uniqid().'
DTSTAMP:20130104T224617Z
DESCRIPTION:'.$this->meeting->agenda.'
SUMMARY:'.$this->meeting->title.'
ATTENDEE:;
PRIORITY:5
CLASS:PUBLIC
END:VEVENT
END:VCALENDAR', 'invite.ics', [
                'mime' => 'text/calendar;charset="utf-8";method=REQUEST',
            ])
            ->view('emails.visitor_invitation_notification', [
                'meeting' => $this->meeting,
                'notifiable' => $notifiable,
                'company_name_ar' => $setting->company_name,
                'company_name_en' => $setting->company_name_en,
                'visit_details_link' => route('visit.code', ['code' => $notifiable->code]),
            ]);
    }

    public function failed()
    {
        $this->guest->update([
            'email_state' => 'error'
        ]);
    }
}
