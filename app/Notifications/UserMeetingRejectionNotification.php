<?php

namespace App\Notifications;

use App\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserMeetingRejectionNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public $meeting, $reason;


    public function __construct($declined_meeting, $decline_reason)
    {
        $this->meeting = $declined_meeting;
        $this->reason = $decline_reason;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $setting = Setting::latest()->first();

        if ($this->meeting->user->language == "ar") {
            $subject = "لقد تم رفض اجتماعكم بعنوان " . $this->meeting->title;
        } else {
            $subject = "Your meeting titled " . $this->meeting->title . " has been declined";
        }

        return (new MailMessage)
            ->subject($subject)
            ->view('emails.meeting_declined', [
                'meeting'         => $this->meeting,
                'reason'          => $this->reason,
                'company_name_ar' => $setting->company_name,
                'company_name_en' => $setting->company_name_en
            ]);
    }
}
