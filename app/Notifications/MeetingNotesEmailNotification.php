<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MeetingNotesEmailNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public $visitors;

    public $visit;

    public function __construct($event)
    {
        $this->visitors = $event->visitors;
        $this->visit = $event->visit;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable){
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($notifiable->language == 'ar') {
            $title = 'محضر الإجتماع' ;
        }else {
            $title = 'Meeting notes' ;
        }

        return (new MailMessage)
            ->subject($title)
            ->view('emails.meeting_notes', [
                'notes' => $this->visit->notes,
            ]);
    }
}
