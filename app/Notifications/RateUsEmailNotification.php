<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Log;

class RateUsEmailNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $visit;

    public function __construct($event)
    {
        $this->visit = $event->visit;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable){
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($notifiable->lang == 'ar') {
            $title = 'قيمنا' ;
        }else {
            $title = 'Rate us' ;
        }
        return (new MailMessage)
            ->subject($title)
            ->view('emails.rate_us', [
                'visit' => $this->visit,
                'visit_rates_link' => route('visit.rates', ['code' => $notifiable->code]),
                'notifiable' => $notifiable,
            ]);
    }
}
