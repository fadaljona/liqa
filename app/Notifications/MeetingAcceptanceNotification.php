<?php

namespace App\Notifications;

use App\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MeetingAcceptanceNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public $meeting;

    public function __construct($accepted_meeting)
    {
        $this->meeting = $accepted_meeting;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $setting = Setting::latest()->first();

        if ($notifiable->language == "ar") {
            $subject = "لقد تم قبول اجتماعكم";
        } else {
            $subject = "your meeting titled " . $this->meeting->title . " has been accepted";
        }

        return (new MailMessage)
            ->subject($subject)
            ->view('emails.meeting_accepted', [
                'meeting'         => $this->meeting,
                'notifiable'      => $notifiable,
                'company_name_ar' => $setting->company_name,
                'company_name_en' => $setting->company_name_en,
            ]);
    }
}
