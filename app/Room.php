<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'rooms';

    protected $fillable = [
        'name_ar',
        'name_en',
        'size',
        'approval',
        'location',
        'floor',
        'direction',
    ];


    public function visits()
    {
        return $this->hasMany(Visit::class, 'room_id');
    }

    public function current_meeting()
    {
        return $this->visits()
            ->whereTime('time_start', '<=', Carbon::now()->toTimeString())
            ->whereTime('time_end', '>=', Carbon::now()->toTimeString())
            ->whereDate('date', date('Y-m-d'))
            ->orderBy('time_start','asc')
            ->whereIn('status', [
                'ready',
                'accepted',
            ])->first();
    }
}