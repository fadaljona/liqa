<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitsRate extends Model
{
    protected $table = 'visits_rates';

    protected $fillable = [
        'cleanness',
        'host',
        'service',
        'text',
        'visit_id',
        'visitor_id',
    ];


    public function visit()
    {
        return $this->belongsTo(Visit::class, 'visit_id');
    }

    public function visitor()
    {
        return $this->belongsTo(Visitor::class, 'visitor_id');
    }
}
