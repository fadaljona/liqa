<?php

namespace App\Providers;

use DB;
use Illuminate\Support\ServiceProvider;
use View;

class ViewComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        View::composer(['layouts.app', 'visits.invitation'], function ($view) {

            $settings = DB::table('settings')->latest('updated_at')->first();

            $view->with('settings', $settings);
        });
    }
}
