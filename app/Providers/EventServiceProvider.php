<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\NotesSaved' => [
            'App\Listeners\SendUserMeetingNotesListener',
        ],
        'App\Events\MeetingEnded' => [
            'App\Listeners\SendVisitorsRatesViaEmailListener',
            'App\Listeners\SendVisitorsRatesViaSmsListener',
        ],
        'App\Events\CheckedInVisitor' => [
            'App\Listeners\NotifyUserVisitorArrivedViaEmailListener',
        ],
        'App\Events\MeetingRejected' => [
            'App\Listeners\MeetingRejectedListener',
        ],

        'App\Events\MeetingAccepted' => [
            'App\Listeners\NotifyUserOfAcceptanceViaEmailListener',
            'App\Listeners\InviteVisitorsViaEmailListener',
            'App\Listeners\InviteVisitorsViaSMSListener',
        ],

        'App\Events\MeetingCancelled' => [
            'App\Listeners\NotifyVisitorsOfCancellationViaEmailListener',
            'App\Listeners\NotifyVisitorsOfCancellationViaSMSListener',
            'App\Listeners\NotifyUserOfCancellationEmailListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
