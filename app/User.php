<?php

namespace App;

use App\Service\Permissions\Permissions;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'type',
        'language'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * @param $route
     *
     * @return bool
     */
    public function canDo($route)
    {
        return Permissions::candDO($this->role, $route);
    }

    public function IsAdmin()
    {
        return $this->role == 'admin';
    }
}
