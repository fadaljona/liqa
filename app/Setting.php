<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';

    protected $fillable = [
        'company_name', // ar
        'company_name_en',
        'company_domain',
        'support_mail',
        'rooms_limits',
        'logo'
    ];

}
