<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRoom extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ar'  => 'required',
            'name_en'  => 'required',
            'size'     => 'required|numeric',
            'approval' => 'required|boolean|in:1,0',
            'floor' => 'required|between:1,20',
            'direction'    => 'required|in:right,left',
            'location_url'    => 'url',
        ];
    }
}
