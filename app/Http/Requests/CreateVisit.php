<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateVisit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title'      => 'required',
            'priority'   => 'required|in:normal,medium,important',
            'date'       => 'required|date|after_or_equal:' . date('Y-m-d'),
            'time_end'   => 'required|date_format:"H:i',
            'time_start' => 'required|date_format:"H:i"|before:time_end',
            'room_id'    => 'required|exists:rooms,id',
        ];

        if (!$this->get('no_visitor')) {
            $rules['visitors.*.mobile'] = 'required|numeric';
            $rules['visitors.*.name']   = 'required';
            $rules['visitors.*.email']  = 'required|email';
            $rules['visitors.*.lang']   = 'required|in:ar,en';
        }


        return $rules;
    }

    public function messages()
    {
        return [
            'visitors.0.mobile' => '',
            'visitors.*.mobile' => '',
        ];
    }
}
