<?php

namespace App\Http\Middleware;

use Closure;
use Route;
use Illuminate\Support\Facades\Auth;

class PermissionCheckAuthenticate
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // check if user has permission to this page
        if (Auth::user()->canDo(Route::currentRouteName())) {
            return $next($request);
        }

        return Redirect()->back()->with('error', trans('messages.access_not_allowed'));
    }
}
