<?php

namespace App\Http\Middleware;

use App;
use Closure;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session('lang') == 'ar' || session('lang') == 'en') {
            app()->setLocale(session('lang'));
        } else {
            app()->setLocale('ar');
        }
        if (auth()->check() && session()->has('lang')) {
            auth()->user()->update(['language' => session('lang')]);
        }

        return $next($request);
    }
}