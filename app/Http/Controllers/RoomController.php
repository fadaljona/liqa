<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRoom;
use App\Room;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('rooms.index', [
            'rooms' => Room::all(),
        ]);
    }


    /**
     * return view for new room
     *
     */

    public function create()
    {
        $rooms_count = Room::count();
        $rooms_limit = Setting::latest()->first()->rooms_limits;

        if ($rooms_count >= $rooms_limit) {
            return redirect(route('rooms.index'))
                ->with('danger', trans('messages.rooms_limits'));
        }
        return view('rooms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRoom $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRoom $request)
    {
        Room::create([
            'name_ar' => $request->get('name_ar'),
            'name_en' => $request->get('name_en'),
            'size' => $request->get('size'),
            'floor' => $request->get('floor'),
            'approval' => $request->get('approval'),
            'location' => $request->get('location'),
            'direction' => $request->get('direction'),
        ]);

        return redirect(route('rooms.index'))
            ->with('success', trans('messages.added_successfully'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('rooms.edit', [
            'room' => Room::findOrFail($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CreateRoom $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateRoom $request, $id)
    {
        $room = Room::findOrFail($id);

        $room->update([
            'name_ar' => $request->get('name_ar'),
            'name_en' => $request->get('name_en'),
            'size' => $request->get('size'),
            'approval' => $request->get('approval'),
            'floor' => $request->get('floor'),
            'location' => $request->get('location'),
            'direction' => $request->get('direction'),
        ]);

        return redirect(route('rooms.index'))
            ->with('success', trans('messages.updated_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Room::destroy($id);

        return redirect(route('rooms.index'))
            ->with('success', trans('messages.deleted_successfully'));
    }

    public function visit($id)
    {
        $room = Room::with([
            'visits' => function ($query) {
                $query->whereIn('status', [
                    'ready',
                    'accepted',
                ]);
                $query->whereDate('date', date('Y-m-d'));
                $query->orderby('time_start', 'asc');
            },
        ])->findOrFail($id);

        return view('rooms.visits', [
            'room' => $room,
            'settings' => Setting::latest()->first(),
            'currentMeeting' => $room->current_meeting()
        ]);

    }

    public function directions($location)
    {

        $rooms = Room::where('floor', $location)->take(5)->get();

        if (count($rooms) == 0) {
            abort(404);
        }

        return view('rooms.directions', [
            'rooms' => $rooms,
            'settings' => Setting::latest()->first(),
            'location' => $location,
        ]);
    }

    public function reservations(Request $request, Room $room)
    {
        $visits = $room->visits->where('date', new Carbon($request->get('meeting_date')));

        $visits_subset = $visits->map(function ($visit) {
            return $visit->only(['time_start', 'time_end']);
        });

        return response()->json($visits_subset);
    }
}
