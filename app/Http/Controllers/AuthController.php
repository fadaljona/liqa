<?php

namespace App\Http\Controllers;

use App\Setting;
use App\User;
use Illuminate\Support\Facades\Auth;
use Moathdev\Office365\Facade\Office365;

class AuthController extends Controller
{
    public function index()
    {
        if (!request()->has('code')) {
            abort(404);
        }

        $code = Office365::getAccessToken(request()->get('code'));

        $user = Office365::getUserInfo($code['token']);

        $setting = Setting::latest()->first();

        if (isset($setting->company_domain)) {
            $result = ends_with($user['mail'], '@' . $setting->company_domain);

            if (!$result) {
                return redirect(route('login'))
                    ->with('error', 'لا تمتلك الصلاحيات الازمه لدخول النظام .');
            }
        }

        $user = User::firstOrCreate([
            'email' => $user['mail'],
        ], [
            'name'     => $user['displayName'],
            'type'     => 'office365',
            'email'    => $user['mail'],
            'password' => 'office365',
        ]);

        Auth::login($user, true);

        return redirect(route('visits.index'));
    }
}
