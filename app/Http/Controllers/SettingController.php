<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSetting;
use App\Setting;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class SettingController extends Controller
{
    public function edit()
    {
        $setting = Setting::latest()->first();

        if (!isset($setting)) {
            $setting = Setting::Create([
                'company_name'     => '',
                'company_domain'   => '',
                'company_name_en'  => '',
                'support_mail'     => '',
                'logo'            => ''
            ]);
        }
        return view('setting.edit', compact('setting'));
    }

    public function update(CreateSetting $request)
    {
        $setting =Setting::findOrfail($request->id);
        if($request->hasFile('logo')){
            File::delete(public_path('images/'.$setting->logo));
            $logo = $request->file('logo');
            $filename = time() . '.' . $logo->getClientOriginalExtension();
            Image::make($logo)->save( public_path('images/'.$filename));
            $setting->logo = $filename;
        };
        $setting->company_name = $request->get('company_name');
        $setting->company_name_en = $request->get('company_name_en');
        $setting->company_domain = $request->get('company_domain');
        $setting->support_mail = $request->get('support_mail');
        $setting->rooms_limits = Setting::latest()->first()->rooms_limits;
        $setting->save();

        return redirect(route('setting.edit'))
            ->with('success', trans('messages.updated_successfully'));
    }

    public function delete($id){
        $setting =  Setting::findOrfail($id);
        File::delete(public_path('images/'.$setting->logo));
        $setting->logo = null;
        $setting->save();
        return redirect()->back();
    }
}
