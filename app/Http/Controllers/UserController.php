<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUser;
use App\Http\Requests\UpdateUser;
use App\User;

class UserController extends Controller
{

    public function index()
    {
        return view('users.index', [
            'users' => User::paginate(15),
        ]);
    }

    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateUser $request
     */
    public function store(CreateUser $request)
    {
        User::create([
            'name'     => $request->get('name'),
            'email'    => $request->get('email'),
            'role'     => $request->get('role'),
            'language' => $request->get('language'),
            'password' => bcrypt($request->get('password')),
        ]);

        return redirect('users')
            ->with('success', trans('messages.added_successfully'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @internal param int $id
     */
    public function edit(User $user)
    {
        if ($user->id != auth()->user()->id && !$user->IsAdmin()) {
            abort(404);
        }

        return view('users.edit', [
            'user' => $user,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUser $request
     * @param User $user
     * @internal param int $id
     */
    public function update(UpdateUser $request, User $user)
    {
        if ($user->id != auth()->user()->id && !$user->IsAdmin()) {
            abort(404);
        }

        $data = [
            'name'     => $request->get('name'),
            'language' => $request->get('language'),
        ];

        if ($request->has('password') && $request->get('password') != null) {
            $data['password'] = bcrypt($request->get('password'));
        }

        if ($user->IsAdmin() && $request->has('email', 'role')) {
            $data['email'] = $request->get('email');
            $data['role']  = $request->get('role');
        }

        $user->update($data);

        return redirect(route('users.index'))
            ->with('success', trans('messages.updated_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        return redirect(route('users.index'))
            ->with('success', trans('messages.deleted_successfully'));
    }
}
