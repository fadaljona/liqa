<?php

namespace App\Http\Controllers;

use App\Exports\MeetingExport;
use App\Room;
use App\User;
use App\Visit;
use App\Visitor;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MeetingsExport;
use App\Exports\VisitorExport;
use App\Exports\UserExport;
use App\Exports\RateExport;
use Carbon\Carbon;
use App\VisitsRate;

class ReportController extends Controller
{

	public function index()
	{
		return view('reports.index', [
			'meetings_count' => Visit::count(),
			'visitors_count' => Visitor::count(),
			'users_count' => User::count(),
            'rates' => VisitsRate::latest()->limit(5)->get(),
			'users' => User::select('users.id', 'users.name', DB::raw('count(visits.id) as number_of_visits'))
				->leftJoin('visits', 'visits.user_id', '=', 'users.id')
				->groupBy('users.id')
				->orderBy('number_of_visits', 'DESC')
				->limit(5)
				->get(),
			'rooms' => Room::select('rooms.id', 'rooms.name_ar', 'rooms.name_en', DB::raw('count(visits.id) as number_of_visits'))
				->leftJoin('visits', 'visits.room_id', '=', 'rooms.id')
				->groupBy('rooms.id')
				->orderBy('number_of_visits', 'DESC')
				->limit(5)
				->get(),
			'count_meetings_by_month' => $this->countMeetingsByMonth(),
            'average_duration' => $this->countAverageDuration(),
		]);
	}

	public function meetingsExport()
    {
        return Excel::download(new MeetingExport(), 'meetings_data.xlsx');
    }

    public function visitorsExport()
    {
        return Excel::download(new VisitorExport(), 'visitors_data.xlsx');
    }

    public function usersExport()
    {
        return Excel::download(new UserExport(), 'users_data.xlsx');
    }

    public function ratesExport()
    {
        return Excel::download(new RateExport(), 'rates_data.xlsx');
    }

	private function countMeetingsByMonth()
	{
		$data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

		$count_orders_by_month = Visit::select('id', 'created_at')
			->orderBy('created_at')
			->get()
			->groupBy(function ($date) {
				return (int)$date->created_at->format('m');
			});

		foreach ($count_orders_by_month as $key => $order) {
			$key = $key - 1;
			$data[$key] = $order->count();
		}

		return app()->chartjs
			->name('count_meeting_by_month')
			->type('bar')
			->labels([
				trans('strings.january'),
				trans('strings.february'),
				trans('strings.march'),
				trans('strings.april'),
				trans('strings.may'),
				trans('strings.june'),
				trans('strings.july'),
				trans('strings.august'),
				trans('strings.september'),
				trans('strings.october'),
				trans('strings.november'),
				trans('strings.december')
			])
			->datasets([
				[
					"label" => trans('strings.total_meetings_per_months'),
					'backgroundColor' => "rgba(38, 185, 154, 0.31)",
					'borderColor' => "rgba(38, 185, 154, 0.7)",
					"pointBorderColor" => "rgba(38, 185, 154, 0.7)",
					"pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
					"pointHoverBackgroundColor" => "#fff",
					"pointHoverBorderColor" => "rgba(220,220,220,1)",
					'data' => $data,
				],
			])
			->options([]);
	}

    public function countAverageDuration()
    {
        $visits = Visit::select('date', 'time_start', 'time_end')->get();

        $totalHours = 0;

        foreach ($visits as $visit){
            $startTime = new Carbon($visit->date->format('Y/m/d').' '.$visit->time_start);
            $endTime = new Carbon($visit->date->format('Y/m/d').' '.$visit->time_end);

            $totalHours += $startTime->diffInHours($endTime);
        }
        return number_format($totalHours* 60 / Visit::count());
    }


}
