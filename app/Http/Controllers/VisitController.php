<?php

namespace App\Http\Controllers;

use App\Events\MeetingAccepted;
use App\Events\MeetingRejected;
use App\Http\Requests\CreateNote;
use App\Http\Requests\CreateVisit;
use App\Http\Requests\CreateRate;
use App\Room;
use App\Setting;
use App\Visit;
use App\Visitor;
use App\VisitsRate;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Events\MeetingCancelled;
use App\Events\CheckedInVisitor;
use App\Events\NotesSaved;
use App\Events\MeetingEnded;

class VisitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (auth()->user()->role == 'user') {
            if ($request->has('sortBy') && $request->get('sortBy') == 'priority') {
                $visits = Visit::whereIn('status', [
                    'ready',
                    'accepted',
                ])
                    ->where('user_id', auth()->user()->id)
                    ->orderBy('date', 'desc')
                    ->orderBy('priority', 'important')
                    ->orderBy('time_start', 'asc')
                    ->paginate(10);
            } elseif ($request->has('sortBy') && $request->get('sortBy') == 'today') {

                $visits = Visit::whereIn('status', [
                    'ready',
                    'accepted',
                ])
                    ->where('user_id', auth()->user()->id)
                    ->whereDate('date', date('Y-m-d'))
                    ->orderBy('time_start', 'dec')
                    ->paginate(10);

            } else {

                $visits = Visit::whereIn('status', [
                    'ready',
                    'accepted',
                ])
                    ->where('user_id', auth()->user()->id)
                    ->orderBy('date', 'desc')
                    ->orderBy('time_start', 'asc')
                    ->paginate(10);
            }
        } else {

            if ($request->has('sortBy') && $request->get('sortBy') == 'priority') {
                $visits = Visit::whereIn('status', [
                    'ready',
                    'accepted',
                ])
                    ->orderBy('date', 'desc')
                    ->orderBy('priority', 'important')
                    ->orderBy('time_start', 'asc')
                    ->paginate(10);
            } elseif ($request->has('sortBy') && $request->get('sortBy') == 'today') {

                $visits = Visit::whereIn('status', [
                    'ready',
                    'accepted',
                ])
                    ->whereDate('date', date('Y-m-d'))
                    ->orderBy('time_start', 'dec')
                    ->paginate(10);

            } else {
                $visits = Visit::whereIn('status', [
                    'ready',
                    'accepted',
                ])
                    ->orderBy(DB::raw('ABS(DATEDIFF(visits.date, NOW()))'))
                    ->orderBy('time_start', 'asc')
                    ->paginate(10);
            }
        }
        return view('visits.index', compact('visits'));
    }

    public function create()
    {
        $rooms = Room::all();

        if ($rooms->count() === 0) {
            return redirect(route('visits.index'))->with('error', trans('messages.add_meeting_room'));
        }

        return view('visits.create', [
            'rooms' => $rooms,
        ]);
    }

    public function store(CreateVisit $request)
    {
        $room = room::findOrFail($request->get('room_id'));

        $visits = Visit::whereDate('date', $request->get('date'))
            ->where('room_id', $room->id)
            ->whereIn('status', [
                'init',
                'accepted',
                'ready',
            ])->get();

        try {
            $visits->each(function ($visit) use ($request) {

                $user_time_start = new Carbon($request->get('date') . $request->get('time_start'));
                $user_time_end = new Carbon($request->get('date') . $request->get('time_end'));

                $meeting_time_start = new Carbon(date('d-m-Y', strtotime($visit->date)) . $visit->time_start);
                $meeting_time_end = new Carbon(date('d-m-Y', strtotime($visit->date)) . $visit->time_end);

                if ($meeting_time_start->between($user_time_start, $user_time_end) || $meeting_time_end->between($user_time_start, $user_time_end)
                    || $user_time_start->between($meeting_time_start, $meeting_time_end) || $user_time_end->between($meeting_time_start, $meeting_time_end)) {
                    throw  new Exception(trans('messages.scheduling_conflict'));
                }
            });
        } catch (Exception $e) {
            return redirect()->back()->withErrors([
                'title' => $e->getMessage(),
            ])->withInput();
        }

        $massage = null;
        if ($room->approval === 1) {
            $status = 'init';
            $massage = trans('messages.request_received');
        } else {
            $status = 'accepted';
            $massage = trans('messages.visit_saved');
        }

        $visit = Visit::create([
            'title' => $request->get('title'),
            'priority' => $request->get('priority'),
            'needs' => $request->get('needs'),
            'date' => $request->get('date'),
            'time_end' => $request->get('time_end'),
            'time_start' => $request->get('time_start'),
            'room_id' => $request->get('room_id'),
            'agenda' => $request->get('agenda'),
            'status' => $status,
            'user_id' => auth()->user()->id,
        ]);

        if (!$request->has('no_visitor')) {

            foreach ($request->get('visitors') as $key => $item) {
                $parking = $item['parking'] ? $item['parking'] : null;
                $email = $item['email'] ? $item['email'] : null;

                $visit->visitor()->create([
                    'name' => $item['name'],
                    'mobile' => $item['mobile'],
                    'lang' => $item['lang'],
                    'email' => $email,
                    'code' => str_random(20),
                    'parking' => $parking,
                ]);
            }
        }

        if ($room->approval != 1) {
            event(new MeetingAccepted($visit));
        }

        return redirect(route('visits.index'))->with('success', $massage);
    }

    public function checkIn($vister_id)
    {
        $visitor = Visitor::findOrFail($vister_id);

        $visitor->update([
            'check_in' => 1,
        ]);

        event(new CheckedInVisitor($visitor->visit->user, $visitor));

        return redirect(route('visits.index'))->with('success', trans('messages.checked_in'));
    }

    public function checkOut($vister_id)
    {
        $visitor = Visitor::findOrFail($vister_id);

        $visitor->update([
            'check_out' => 1,
        ]);

        event(new MeetingEnded($visitor->visit));

        return redirect(route('visits.index'))->with('success', trans('messages.checked_out'));
    }

    public function createNote(Visit $visit)
    {

        if (auth()->user()->id === $visit->user->id) {
            return view('visits.note', compact('visit'));
        }

        return redirect(route('visits.index'))
            ->with('error', trans('messages.access_not_allowed'));
    }

    public function storeNote(CreateNote $request, Visit $visit)
    {
        $visit->update([
            'notes' => $request->get('notes'),
        ]);

        event(new NotesSaved($visit, $visit->visitor));

        return redirect(route('visits.index'))
            ->with('success', trans('messages.notes_saved'));
    }

    public function visitCode($code)
    {
        $visitor = Visitor::with('visit')->where('code', $code)->firstOrFail();

        $visitor->update([
            'view_code' => true,
        ]);

        return view('visits.invitation', [
            'visitor' => $visitor,
            'setting' => Setting::latest()->first()
        ]);
    }

    public function rateUs($code)
    {
        $visitor = Visitor::where('code', $code)->firstOrFail();

        return view('visits.rates', [
            'code' => $visitor->code,
            'visitor' => $visitor
        ]);
    }

    public function storeRate(CreateRate $request, $code)
    {
        $visitor = Visitor::with('visit')->where('code', $code)->firstOrFail();


        if (!$visitor->has_rated) {
            VisitsRate::create([
                'cleanness' => $request->get('cleanness'),
                'host' => $request->get('host'),
                'service' => $request->get('service'),
                'text' => $request->get('text'),
                'visit_id' => $visitor->visit->id,
                'visitor_id' =>$visitor->id,
            ]);
            $visitor->update([
                'has_rated' => 1,
            ]);

            return redirect(route('visit.rates', ['code' => $code]))
                ->with('success', trans('messages.rated_successfully'));
        } else {
            return redirect(route('visit.rates', ['code' => $code]))
                ->with('error', trans('messages.already_rated'));
        }

    }

    public function init()
    {
        return view('visits.init', [
            'visits' => visit::where('status', 'init')->paginate(15),
        ]);

    }

    public function accepted($id)
    {
        $visit = visit::where('status', 'init')->findorfail($id);

        $visit->update([
            'status' => 'accepted',
        ]);

        event(new MeetingAccepted($visit));


        return redirect(route('visits.init'))
            ->with('success', trans('messages.visit_accepted'));

    }

    public function decline(Request $request, $id)
    {
        $visit = visit::where('status', 'init')->findorfail($id);

        $visit->update([
            'status' => 'refused',
        ]);

        event(new MeetingRejected($visit, $request->get('reason')));

        return redirect(route('visits.init'))
            ->with('success', trans('messages.visit_declined'));

    }

    public function cancel(Visit $visit)
    {
        if ((!(auth()->user()->IsAdmin()) && (auth()->user()->id != $visit->user->id)) || !($visit->cancellable())) {
            abort(404);
        }

        event(new MeetingCancelled($visit));

        $visit->delete();

        return redirect(route('visits.index'))->with('success', trans('messages.cancelled_successfully'));
    }
}

