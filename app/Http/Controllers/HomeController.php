<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('visits.index'));
    }

    public function changeLocal($local)
    {

        if (in_array($local, [
            'ar',
            'en',
        ])) {
            session(['lang' => $local]);
            return redirect()->back();
        }else{
            abort(404);
        }

    }
}
