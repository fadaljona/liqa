@extends('layouts.app')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="box">
            <div class="panel-body">
                <h3>@lang('validation.attributes.edit_user')</h3>
                <br>
                <form method="post" action="{{ route('users.update', $user->id) }}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="put"/>
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.name')</label>
                        <div class="col-md-8"><input value="{{$user->name}}" type="text" name="name"
                                                     class="form-control" required autofocus/></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.password')</label>
                        <div class="col-md-8">
                            <input type="password" name="password" class="form-control"
                                   autofocus/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.email')</label>
                        <div class="col-md-8"><input value="{{$user->email}}" type="email" name="email"
                                                     class="form-control" @if($user->role != "admin") readonly @endif/></div>
                        <div class="clearfix"></div>
                    </div>
                    @if(auth()->user()->role == "admin")
                        <div class="form-group">
                            <label class="col-md-4">@lang('validation.attributes.role')</label>
                            <div class="col-md-8">
                                <select name="role" class="form-control" required autofocus>
                                    <option value="user"
                                            @if(old('role', $user->role) =='admin') selected @endif>@lang('validation.attributes.standard_user')</option>
                                    <option value="reception"
                                            @if(old('role', $user->role) =='reception') selected @endif>@lang('validation.attributes.reception_user')</option>
                                    <option value="admin"
                                            @if(old('role', $user->role) =='admin') selected @endif>@lang('validation.attributes.admin_user')</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.language')</label>
                        <div class="col-md-8">
                            <select name="language" class="form-control" required autofocus>
                                <option value="">@lang('validation.attributes.please_select')</option>
                                <option value="ar" @if(old('language', $user->language) == "ar") selected @endif>@lang('validation.attributes.ar')</option>
                                <option value="en" @if(old('language', $user->language) == "en") selected @endif>@lang('validation.attributes.en')</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="@lang('validation.attributes.save')"
                               class="btn visitors-theme-button saveBtn pull-left"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection