@extends('layouts.app')

@section('content')
    <div class="row page-title">
        <div class="col-md-12">

            <div class="col-md-6" style="text-align: left;">

            </div>
        </div>
        <div class="col-md-8 col-xs-8"><h3>@lang('validation.attributes.users')</h3></div>
        <div class="col-md-4 col-xs-4"><a class="visitors-theme-button" id="newUserBtn" href="/users/create"><i
                        class="fas fa-plus"></i>@lang('validation.attributes.add')</a></div>
    </div>

    <div class="box" id="usersContainer">
        <table class="table users-table" id="usersListTable">
            <thead>
            <tr>
            <th style="text-align: center">@lang('validation.attributes.name')</th>
            <th style="text-align: center">@lang('validation.attributes.email')</th>
            <th style="text-align: center">@lang('validation.attributes.role')</th>
            <th style="text-align: center">@lang('validation.attributes.language')</th>
            <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)

                <tr>
                    <td style="text-align: center">{{$user->name}}</td>
                    <td style="text-align: center">{{$user->email}}</td>
                    <td style="text-align: center">@lang('validation.attributes.'.$user->role)</td>
                    <td style="text-align: center">@lang('validation.attributes.' . $user->language)</td>
                    <td class="text-left">
                        <a class="btn btn-sm" href="/users/{{$user->id}}/edit">@lang('validation.attributes.edit')</a>
                        <button class="btn btn-sm btn-danger btn-delete" data-toggle="modal"
                                data-target="#delete-modal-{{$user->id}}">@lang('validation.attributes.delete')</button>
                        <form method="post" action="/users/{{$user->id}}">
                            <input type="hidden" name="_method" value="delete"/>
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$user->id}}"/>

                            <!-- Modal HTML -->
                            <div id="delete-modal-{{$user->id}}" class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">@lang('messages.delete_confirmation')</h4>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default"
                                                    data-dismiss="modal">@lang('validation.attributes.no')</button>
                                            <button type="button" class="btn btn-primary"
                                                    onclick="$(this).closest('form').submit();">@lang('validation.attributes.yes')</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div style="text-align: center">
        {{ $users->appends(Request::except('page'))->links() }}
    </div>
@endsection
@section('js')
    <script>
        $("#usersListTable").DataTable({
            language: {
                search: "@lang('validation.attributes.search')",
                Show: "@lang('validation.attributes.show')",
                entries: "@lang('validation.attributes.entries')",
                info: "@lang('validation.attributes.show') _END_ @lang('validation.attributes.out_of') _TOTAL_",
                lengthMenu: "@lang('validation.attributes.show') _MENU_ @lang('validation.attributes.entries')",
                paginate: {
                    next: "@lang('validation.attributes.next')",
                    previous: "@lang('validation.attributes.previous')"
                }
            }
        });
    </script>
@endsection