@extends('layouts.app')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="box">
            <div class="panel-body">
                <h3>@lang('validation.attributes.add_new_user')</h3>
                <br>
                <form method="post" action="{{ route('users.store')}}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.name')</label>
                        <div class="col-md-8">
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control" required
                                   autofocus/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.password')</label>
                        <div class="col-md-8">
                            <input type="password" name="password" class="form-control" required
                                   autofocus/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.email')</label>
                        <div class="col-md-8">
                            <input type="text" value="{{ old('email') }}" name="email" class="form-control" autofocus/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.role')</label>
                        <div class="col-md-8">
                            <select name="role" class="form-control" required autofocus>
                                <option value="">@lang('validation.attributes.please_select')</option>
                                <option value="user"
                                        @if(old('role') =='admin') selected @endif>@lang('validation.attributes.standard_user')</option>
                                <option value="reception"
                                        @if(old('role') =='reception') selected @endif>@lang('validation.attributes.reception_user')</option>
                                <option value="admin"
                                        @if(old('role') =='admin') selected @endif>@lang('validation.attributes.admin_user')</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.language')</label>
                        <div class="col-md-8">
                            <select name="language" class="form-control" required autofocus>
                                <option value="">@lang('validation.attributes.please_select')</option>
                                <option value="ar" @if(old('language') == "ar") selected @endif>@lang('validation.attributes.ar')</option>
                                <option value="en" @if(old('language') == "en") selected @endif>@lang('validation.attributes.en')</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="@lang('validation.attributes.add')"
                               class="btn visitors-theme-button saveBtn pull-left"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection