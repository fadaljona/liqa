<!DOCTYPE html>
@php
    app()->setLocale($visitor->lang);
@endphp
<html lang="{{ $visitor->lang }}">
<head>
    <title>@lang('validation.attributes.meeting_details')</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    @if($visitor->lang == "ar")
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-flipped.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.css">
    @endif
    <style>
        .rate {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rate:not(:checked) > input {
            position: absolute;
            top: -9999px;
        }

        .rate:not(:checked) > label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rate:not(:checked) > label:before {
            content: '★ ';
        }

        .rate > input:checked ~ label {
            color: #ffc700;
        }

        .rate:not(:checked) > label:hover,
        .rate:not(:checked) > label:hover ~ label {
            color: #deb217;
        }

        .rate > input:checked label:hover,
        .rate > input:checked label:hover ~ label,
        .rate > input:checked ~ label:hover,
        .rate > input:checked ~ label:hover ~ label,
        .rate > label:hover ~ input:checked ~ label {
            color: #c59b08;
        }

        .panel-logo {
            text-align: center;
        }

        .panel-logo img {
            height: 150px;
        }

        .visitors-theme-button {
            color: #fff !important;
            background: #234196;
            text-decoration: none;
        }

        .visitors-theme-button svg {
            margin-left: 5px;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="panel panel-logo">
        @if(isset($settings->logo))
            <img class="panel-body panel-logo" src="{{asset('images/'.$settings->logo)}}" alt="logo">
        @else
            <img class="panel-body panel-logo" src="{{asset('images/logo.png')}}" alt="logo">
        @endif
    </div>
    <hr/>
    <div class="panel panel-default">
        @if(session()->has('success'))
            <div class="alert alert-success">
                <a href="#" class="close" aria-label="close">&times;</a>
                <i class="fa fa-check"></i> {!! session()->get('success') !!}
            </div>
        @endif
        @if(session()->has('danger'))
            <div class="alert alert-danger">
                <a href="#" class="close" aria-label="close">&times;</a>
                {!! session()->get('danger') !!}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="panel-body">
            @if(!$visitor->has_rated)
                <form id="rate-us-form" action="{{route('visits.storeRate', $code)}}"
                      method="post">
                    {{ csrf_field() }}
                    <div style="margin-bottom: 70px">
                        <div>
                            <p>@if($visitor->lang == "ar")
                                    كيف كان المكان وقاعة الاجتماعات ؟
                                @else How was the place & meeting room
                                    clean? @endif</p>
                        </div>
                        <div class="rate @if(app()->getLocale() == "ar")  pull-left @endif">
                            <input type="radio" id="star5" name="cleanness" value="5"/>
                            <label for="star5" title="text">5 stars</label>
                            <input type="radio" id="star4" name="cleanness" value="4"/>
                            <label for="star4" title="text">4 stars</label>
                            <input type="radio" id="star3" name="cleanness" value="3"/>
                            <label for="star3" title="text">3 stars</label>
                            <input type="radio" id="star2" name="cleanness" value="2"/>
                            <label for="star2" title="text">2 stars</label>
                            <input type="radio" id="star1" name="cleanness" value="1"/>
                            <label for="star1" title="text">1 star</label>
                        </div>
                    </div>
                    <div style="margin-bottom: 70px">
                        <div>
                            <p>@if(app()->getLocale() == "ar") كيف كان تعامل المستضيف@else How was the host? @endif</p>
                        </div>
                        <div class="rate @if(app()->getLocale() == "ar")  pull-left @endif">
                            <input type="radio" id="starr5" name="host" value="5"/>
                            <label for="starr5" title="text">5 stars</label>
                            <input type="radio" id="starr4" name="host" value="4"/>
                            <label for="starr4" title="text">4 stars</label>
                            <input type="radio" id="starr3" name="host" value="3"/>
                            <label for="starr3" title="text">3 stars</label>
                            <input type="radio" id="starr2" name="host" value="2"/>
                            <label for="starr2" title="text">2 stars</label>
                            <input type="radio" id="starr1" name="host" value="1"/>
                            <label for="starr1" title="text">1 star</label>
                        </div>
                    </div>
                    <div style="margin-bottom: 70px">
                        <div>
                            <p>@if(app()->getLocale() == "ar")
                                    كيف كانت الخدمة (قهوة ، شاي ،
                                    ..)؟
                                @else How was the service(coffee, tea,
                                    etc.)?@endif</p>
                        </div>
                        <div class="rate @if(app()->getLocale() == "ar")  pull-left @endif">
                            <input type="radio" id="starrr5" name="service" value="5"/>
                            <label for="starrr5" title="text">5 stars</label>
                            <input type="radio" id="starrr4" name="service" value="4"/>
                            <label for="starrr4" title="text">4 stars</label>
                            <input type="radio" id="starrr3" name="service" value="3"/>
                            <label for="starrr3" title="text">3 stars</label>
                            <input type="radio" id="starrr2" name="service" value="2"/>
                            <label for="starrr2" title="text">2 stars</label>
                            <input type="radio" id="starrr1" name="service" value="1"/>
                            <label for="starrr1" title="text">1 star</label>
                        </div>
                    </div>
                    <div>
                        <p>@if(app()->getLocale() == "ar") ملاحظات أخرى :@else Free text: @endif</p>
                        <textarea name="text" cols="20" rows="5"
                                  placeholder="@if(app()->getLocale() == "ar") ملاحظات أخرى @else Free text @endif"></textarea>
                    </div>
                    <div>
                        <input type="submit" class="btn visitors-theme-button saveBtn pull-left"
                               style="margin-bottom: 23px;" value="@lang('validation.attributes.save')"/>
                    </div>
                </form>
            @else
                <p> @lang('messages.already_rated')</p>
            @endif
        </div>
    </div>
</div>
<footer id="footer" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                Powered By <a href="https://land.Liqa.io">Liqa.io</a>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
