<!DOCTYPE html>
@php
    app()->setLocale($visitor->lang);
@endphp
<html lang="{{ app()->getLocale() }}">
<head>
    <title>@lang('validation.attributes.meeting_details')</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://kit.fontawesome.com/7f3843191f.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    @if($visitor->lang == "ar")
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-flipped.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.css">
    @endif
    <style>
        .logo {
            height: 60px;
            margin-top: 20px;
        }

        .panel-logo {
            text-align: center;
        }

        .panel-logo img {
            height: 150px;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="panel panel-logo">
        @if(isset($settings->logo))
            <img class="panel-body panel-logo" src="{{asset('images/'.$settings->logo)}}" alt="logo">
        @else
            <img class="panel-body panel-logo" src="{{asset('images/logo.png')}}" alt="logo">
        @endif
    </div>
    <hr/>
    @if(isset($visitor->visit->agenda))
        <div class="panel panel-default">
            <div class="panel-body"><b><i class="fas fa-sticky-note"></i> @lang('validation.attributes.agenda') :</b>
                <p>{!!  $visitor->visit->agenda  !!}</p>
            </div>
        </div>
    @endif
    <div class="panel panel-default">
        <div class="panel-body"><b><i class="fas fa-calendar-alt"></i> @lang('validation.attributes.meeting_date')
                :</b> {{$visitor->visit->date->format('Y-m-d')}}</div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body"><b><i class="fas fa-clock"></i> @lang('validation.attributes.meeting_time')
                :</b> {{  date_format(date_create($visitor->visit->time_start),"h:i") }}
            - {{  date_format(date_create($visitor->visit->time_end),"h:i") }}</div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body"><b><i class="fas fa-location-arrow"></i> @lang('validation.attributes.meeting_location') :</b>
            @if(app()->islocale('en'))
                {{$visitor->visit->room->name_en}}
            @else
                {{$visitor->visit->room->name_ar}}
            @endif
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body"><b><i class="fas fa-map-marked-alt"></i> @lang('validation.attributes.location') :</b>
            <a href="{{ $visitor->visit->room->location }}" target="_blank">
                @lang('validation.attributes.google_map')</a></div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body"><b><i class="fas fa-building"></i> @lang('validation.attributes.floor') :</b> {{ $visitor->visit->room->floor }}</div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body"><i class="fas fa-user"></i> <b>@lang('validation.attributes.host') :</b>
            <ul>
                <li>{{$visitor->visit->user->name}}</li>
                <li>{{$visitor->visit->user->email}}</li>
            </ul>
        </div>
    </div>
</div>
<footer id="footer" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                Powered By <a href="https://land.Liqa.io">Liqa.io</a>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
