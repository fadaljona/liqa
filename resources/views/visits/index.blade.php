@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @if(session()->has('error'))
                <div class="alert alert-danger">
                    <a href="#" class="close" aria-label="close">&times;</a>
                    {!! session()->get('error') !!}
                </div>
            @endif
            <div class="col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="col-md-8 col-xs-6">
                        <div class="visits-sorting">
                            <span>@lang('validation.attributes.sort_by') :&nbsp;&nbsp;</span>
                            <ul @if(app()->getLocale() == 'ar') class="pull-right" style="padding-right: 0px"
                                @else class="pull-left left-position" @endif>
                                <li><a href="{{ route('visits.index') }}"
                                       @if(!request()->has('sortBy'))  class="current" @endif >@lang('validation.attributes.closest')</a>
                                </li>
                                <li><a href="{{ route('visits.index', [ 'sortBy' => 'priority']) }}"
                                       @if(request()->has('sortBy') && request()->get('sortBy') == 'priority')  class="current" @endif>@lang('validation.attributes.priority_level')</a>
                                </li>
                                <li>
                                    <a href="{{ route('visits.index', [ 'sortBy' => 'today']) }}"
                                       @if(request()->has('sortBy') && request()->get('sortBy') == 'today')  class="current" @endif>@lang('validation.attributes.today')</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-6">
                        <a href="{{ route('visits.create') }}" class="visitors-theme-button text-center"
                           id="main-visit-btn"><i
                                    class="fas fa-plus"></i> @lang('validation.attributes.create_meeting')</a>
                    </div>

                </div>
                @if($visits->count() > 0)
                    @foreach ($visits as $visit)
                        <div class="box">
                            <div class="box-title">
                                @if($visit->isPast() && auth()->user()->id === $visit->user->id)
                                    <a href="{{route('visits.createNote', $visit->id)}}"
                                       class="btn btn-primary  @if(app()->getLocale() == 'ar') pull-left @else pull-right @endif">
                                        <i class="fas fa-sticky-note"></i>
                                        @if(isset($visit->notes))
                                            @lang('validation.attributes.minutes_of_meeting')
                                        @else
                                            @lang('validation.attributes.write_minutes_of_meeting')
                                        @endif
                                    </a>
                                @endif
                                @if($visit->cancellable())
                                    <button type="button"
                                            class="btn btn-danger @if(app()->getLocale() == "ar") pull-left @else pull-right @endif"
                                            data-toggle="modal" data-target="#meeting-cancellation-prompt-{{$visit->id}}">
                                        x
                                    </button>
                            @endif
                            <!-- Modal -->
                                <div class="modal fade" id="meeting-cancellation-prompt-{{$visit->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title"
                                                    id="myModalLabel">@lang('messages.cancellation_confirmation')</h4>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default"
                                                        data-dismiss="modal">@lang('validation.attributes.no')</button>
                                                <button type="button"
                                                        data-id="{{$visit->id}}"
                                                        class="btn btn-primary meeting-cancellation-btn">@lang('validation.attributes.yes')</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <form id="meeting-cancellation-form-{{$visit->id}}"
                                      action="{{route('visits.cancel', $visit)}}" method="post">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                </form>

                                <h3>{{$visit->title}}</h3>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <ul class="col-md-12 visit-info">
                                        <li>@lang('validation.attributes.priority_level') :
                                            @lang("validation.attributes.priority.".$visit->priority)
                                        </li>
                                        <li><i class="far fa-calendar-alt"></i>{{ $visit->date->format('Y-m-d') }}</li>
                                        <li>
                                            <i class="far fa-clock"></i>{{  date_format(date_create($visit->time_start),"h:i") }}
                                            - {{  date_format(date_create($visit->time_end),"h:i") }}</li>
                                        <li><i class="fas fa-map-marker"></i>
                                            @if(app()->islocale('en'))
                                                {{$visit->room->name_en}}
                                            @else
                                                {{$visit->room->name_ar}}
                                            @endif
                                        </li>
                                        <li><i class="fas fa-male"
                                               title="@lang('validation.attributes.host')"></i> {{$visit->user->name}}
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <h4>@lang('validation.attributes.guests')</h4>
                                    @if($visit->visitor->count() == 0)
                                        <span>@lang('validation.attributes.no_visitors')</span>
                                        <hr/>
                                        <br/>
                                    @else
                                        @foreach($visit->visitor as $visitor)
                                            <div class="row">
                                                <div class="col-md-6 col-xs-12">
                                                    <span class="fa fa-user"></span>
                                                    {{$visitor->name}}
                                                </div>
                                                <div class="col-md-6 col-xs-12">
                                                    <span class="fa fa-phone"></span>
                                                    {{$visitor->mobile}}
                                                </div>
                                                <div class="col-md-6 col-xs-12">
                                                    <span class="fa fa-envelope"></span>
                                                    {{$visitor->email}}
                                                </div>
                                                <div class="col-md-6 col-xs-12">
                                                    <span class="fa fa-car"></span>
                                                    {{$visitor->parking ? $visitor->parking : trans('validation.attributes.no_parking')}}
                                                </div>
                                                <div class="col-md-6 col-xs-12">
                                                    @if(!$visitor->check_in)
                                                        <span class="fa fa-user-times"></span>
                                                        &nbsp;@lang('validation.attributes.no_arrival')
                                                    @elseif(!$visitor->check_out && $visitor->check_in)
                                                        <span><i class="fas fa-check-circle"></i>@lang('validation.attributes.arrival')</span>
                                                    @elseif($visitor->check_out && $visitor->check_in)
                                                        <span> @lang('validation.attributes.checkout')
                                                                : {{ $visitor->updated_at }}</span>
                                                    @endif
                                                </div>
                                                <div class="col-md-6 col-xs-12">
                                                    @if(!$visitor->check_in)
                                                        @if(auth()->user()->canDo('visits.checkIn'))
                                                            <a href="{{ route('visits.checkIn', [ $visitor->id]) }}"
                                                               class="btn btn-sm">
                                                                @lang('validation.attributes.check_in')
                                                            </a>
                                                        @endif
                                                    @elseif(!$visitor->check_out && $visitor->check_in)
                                                        <a href="{{ route('visits.checkOut', $visitor->id) }}"
                                                           class="btn btn-sm">
                                                            @lang('validation.attributes.checkout')
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                            <hr/>
                                            <br/>
                                        @endforeach
                                    @endif
                                    @if(isset($visit->needs))
                                        <h4>@lang('validation.attributes.needs') : </h4>
                                        <p>{{ $visit->needs }}</p>
                                    @endif
                                    <br/>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="text-center">
                        {{ $visits->appends(Request::except('page'))->links() }}
                    </div>
                @else
                    <div class="box">
                        <div class="no-data">@lang('validation.attributes.no_data')</div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $(".meeting-cancellation-btn").click(function () {
                var visitID = $(this).attr('data-id');
                $('#meeting-cancellation-form-' + visitID).submit();
            });
        });
    </script>
@endsection
