@extends('layouts.app')

@section('content')
    <div class="page-title">
        <h3>@lang('validation.attributes.create_meeting')</h3>
    </div>
    <div class="row">
        <form class="form-horizontal" method="POST" action="{{ route('visits.store') }}" id="visitForm">
            {{ csrf_field() }}
            <div class="col-md-6">
                <div class="box">
                    <div class="panel-body">
                        <div class="main-form">
                            <div class="form-group">
                                <label class="col-md-4">@lang('validation.attributes.visit_title')</label>
                                <div class="col-md-8">
                                    <input type="text" value="{{ old('title') }}" name="title" class="form-control"
                                           required autofocus/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4">@lang('validation.attributes.priority_level')</label>
                                <div class="col-md-8">
                                    <select name="priority" class="form-control">
                                        <option @if(old('priority') == "normal") selected
                                                @endif value="normal">@lang('validation.attributes.normal_priority')</option>
                                        <option @if(old('priority') == "medium") selected
                                                @endif value="medium">@lang('validation.attributes.medium_priority')</option>
                                        <option @if(old('priority') == "important") selected
                                                @endif value="important">@lang('validation.attributes.important_priority')</option>
                                    </select></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4">@lang('validation.attributes.date')</label>
                                <div class="col-md-8">
                                    <input type="date" id="meeting-date" value="{{ old('date', date('Y-m-d')) }}"
                                           name="date"
                                           class="form-control" required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4">@lang('validation.attributes.time')</label>
                                <div class="col-md-4">
                                    <label>@lang('validation.attributes.from')</label>
                                    <input type="time" value="{{ old('time_start', '00:00') }}" name="time_start"
                                           class="form-control" required/>
                                </div>
                                <div class="col-md-4">
                                    <label>@lang('validation.attributes.to')</label>
                                    <input type="time" value="{{ old('time_end', '00:00') }}" name="time_end"
                                           class="form-control" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="agenda" class="col-md-4">@lang('validation.attributes.agenda')
                                    (@lang('validation.attributes.optional'))</label>
                                <div class="col-md-8">
                                    <textarea name="agenda" rows="10" id="agenda"
                                              class="form-control">{{ old('agenda') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="needs" class="col-md-4">@lang('validation.attributes.needs')(@lang('validation.attributes.optional'))</label>
                                <div class="col-md-8">
                                    <textarea name="needs" rows="3" id="needs" class="form-control">{{ old('needs') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4">@lang('validation.attributes.meeting_rooms')</label>

                                <div class="col-md-8">
                                    <select name="room_id" id="room-id" class="form-control" required>
                                        <option value="">@lang('validation.attributes.select_meeting_room')</option>
                                        @foreach($rooms as $room)
                                            <option @if(old('room_id') == $room->id) selected
                                                    @endif value="{{$room->id}}">
                                                @if(app()->isLocale('en'))
                                                    {{$room->name_en}} - {{ $room->size }} person
                                                @else
                                                    {{$room->name_ar}} -  {{ $room->size }} @if($room->size > 10)
                                                        مقعد @else مقاعد  @endif
                                                @endif
                                            </option>
                                        @endforeach
                                    </select></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4">@lang('validation.attributes.host')</label>
                                <div class="col-md-8">{{ auth()->user()->name }}</div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="box toggled" id="reserved-times-container">
                    <div class="panel-heading">
                        <h3 class="panel-title">@lang('validation.attributes.reserved_times')</h3>
                    </div>
                    <div class="panel-body" id="reserved-times"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box" style="margin-top:20px;">
                    <div class="panel-heading">
                        <label for="no_visitor"><input
                                    type="checkbox"
                                    name="no_visitor"
                                    @if(old('no_visitor')) checked @endif
                                    value="true"
                                    id="no_visitor_check">
                            <span>@lang('validation.attributes.no_visitor')</span>
                        </label>
                    </div>
                </div>
                <div id="guestsPanel">
                    @if(old('visitors') > 0)
                        @foreach(old('visitors') as $key => $visitor)
                            <div class="box guest-card" id="guestCard-{{$key}}"
                                 style="margin-top:20px; margin-bottom:20px;">
                                <div class="panel-heading"><h3
                                            class="panel-title">@lang("validation.attributes.guest")  </h3>
                                    @if(app()->getLocale() == "ar")
                                        <button type='button' id="{{$key}}" class='guest-delete-btn-ar'>X</button>
                                    @else
                                        <button type='button' id="{{$key}}" class='guest-delete-btn'>X</button>
                                    @endif
                                </div>
                                <div class="panel-body">
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <input class="form-control" name="visitors[{{$key}}][name]" type="text"
                                                   value="{{$visitor['name']}}" required>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="visitors[{{$key}}][mobile]" class="form-control"
                                                   value="{{$visitor['mobile']}}" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <input type="email" name="visitors[{{$key}}][email]" class="form-control"
                                                   value="{{$visitor['email']}}" required>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="visitors[{{$key}}][parking]" class="form-control"
                                                   value="{{$visitor['parking']}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <select name="visitors[{{$key}}][lang]" class="form-control" required>
                                                <option value="">@lang("validation.attributes.select_visitor_language")</option>
                                                <option @if($visitor['lang'] == 'ar') selected
                                                        @endif value="ar">@lang("validation.attributes.ar")</option>
                                                <option @if($visitor['lang'] == 'en') selected
                                                        @endif  value="en">@lang("validation.attributes.en")</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @elseif(!old('no_visitor'))
                        <div class="box guest-card" id="guestCard-0" style="margin-top:20px; margin-bottom:20px;">
                            <div class="panel-heading"><h3
                                        class="panel-title">@lang("validation.attributes.guest") </h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input class="form-control" name="visitors[0][name]" type="text"
                                               placeholder="@lang('validation.attributes.name')" required>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="visitors[0][mobile]" class="form-control"
                                               placeholder="9665xxxxxxxxx" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input type="email" name="visitors[0][email]" class="form-control"
                                               placeholder="@lang('validation.attributes.email')" required>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="visitors[0][parking]" class="form-control"
                                               placeholder="@lang("validation.attributes.parking_number") (@lang('validation.attributes.optional'))">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <select name="visitors[0][lang]" class="form-control" required>
                                            <option value="">@lang("validation.attributes.select_visitor_language")</option>
                                            <option value="ar">@lang("validation.attributes.ar")</option>
                                            <option value="en">@lang("validation.attributes.en")</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <button type="button" id="moreGuestsBtn" class="btn btn-block visitors-theme-button"
                        style="margin-top: 20px; margin-bottom: 20px;"><span
                            class="fa fa-plus"></span>&nbsp;@lang('validation.attributes.add_more')</button>
                <div><input type="submit" class="btn visitors-theme-button saveBtn pull-left"
                            style="margin-bottom: 23px;" value="@lang('validation.attributes.save')"/>
                </div>
            </div>
        </form>
    </div>
    @include('visits.partials.guest_card')
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                }
            });

            $("#meeting-date, #room-id").change(function () {
                if ($("#meeting-date").val() != null || $("#room-id").val() != "") {
                    var fd = new FormData();
                    fd.append("meeting_date", $("#meeting-date").val());
                    fd.append('_token', "{{ csrf_token() }}");
                    $.ajax({
                        type: 'POST',
                        url: '/rooms/' + $("#room-id").val() + '/reservations',
                        data: fd,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (Object.keys(data).length > 0) {
                                $("#reserved-times").empty();
                                $("#reserved-times").append(
                                    '<table class="table table-bordered">' +
                                    '<thead>' +
                                    '<tr>' +
                                    '<th>@lang("validation.attributes.from")' +
                                    '<th>@lang("validation.attributes.to")' +
                                    '</tr>' +
                                    '</thead>' +
                                    '<tbody>'
                                );

                                $.each(data, function (index, visitObject) {
                                    $("#reserved-times table tbody").append(
                                        '<tr>' +
                                        '<td>' + visitObject.time_start + '</td>' +
                                        '<td>' + visitObject.time_end + '</td>' +
                                        '</tr>'
                                    );
                                });

                                $("#reserved-times").append(
                                    '</tbody>' +
                                    '</table>'
                                );
                                if ($("#reserved-times-container").css('display') == "none") {
                                    $("#reserved-times-container").toggle();
                                }
                            } else {
                                $("#reserved-times").empty();
                                $("#reserved-times").append("<h4 class='text-center'>@lang('validation.attributes.no_reserved_times')</h4>");
                                if ($("#reserved-times-container").css('display') == "none") {
                                    $("#reserved-times-container").toggle();
                                }
                            }
                        },
                        error: function () {
                            $("#reserved-times").empty();
                            if ($("#reserved-times-container").css('display') == "block") {
                                $("#reserved-times-container").toggle();
                            }
                        }
                    });
                }

            });
        });
    </script>
    <script src="{{ asset('lib/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace('agenda', {
            @if(app()->getLocale() == 'ar')   contentsLangDirection: 'rtl' @endif
        });
    </script>
@endsection
