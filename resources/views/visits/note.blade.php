@extends('layouts.app')

@section('content')
    <div class="row page-title">
        <div class="col-md-12">
            <h3 class="col-md-6">@lang('validation.attributes.minutes_of_meeting')</h3>
        </div>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div class="box">
            <div class="panel-body">
                <form id="saving-notes-form-{{$visit->id}}" action="{{route('visits.storeNote', $visit->id)}}"
                      method="post">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="notes" class="col-md-4">@lang('validation.attributes.write_minutes_of_meeting')</label>
                        <div class="col-md-8">
                            @if(!isset($visit->notes))
                                <textarea rows="8" type="text" name="notes" id="notes" class="form-control" autofocus required>{{ old('notes', $visit->notes) }}</textarea>
                            @else
                                {!! $visit->notes !!}
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
                @if(!isset($visit->notes))
                <button type="button"
                        class="btn visitors-theme-button @if(app()->getLocale() == "ar") pull-left @else pull-right @endif"
                        data-toggle="modal" data-target="#saving-notes-prompt">
                    @lang('validation.attributes.save')
                </button>
                @endif
            </div>
            <div class="modal fade" id="saving-notes-prompt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">
                                @lang('messages.saving_notes_confirmation')
                            </h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal">@lang('validation.attributes.no')</button>
                            <button type="button" id="saving-notes-btn" data-id="{{$visit->id}}"
                                    class="btn btn-primary">@lang('validation.attributes.yes')</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('lib/ckeditor/ckeditor.js') }}"></script>
    <script>
        $(document).ready(function () {
            $("#saving-notes-btn").click(function () {
                var visitID = $(this).attr('data-id');
                $('#saving-notes-form-' + visitID).submit();
            });
        });
    </script>
    <script>
        CKEDITOR.replace('notes', {
            @if(app()->getLocale() == 'ar')   contentsLangDirection: 'rtl' @endif
        });
    </script>
@endsection

