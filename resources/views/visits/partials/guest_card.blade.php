<script>

    $(document).ready(function () {

        if (document.getElementById("no_visitor_check").checked) {
            $("#moreGuestsBtn").css('display', "none");
        }

        var currentLocale = $("meta[name=locale]").attr("content");

                @if(old('visitors') > 0)
        var numberOfGuests = {{ count(old('visitors')) + 1 }};
                @else
        var numberOfGuests = 1;
        @endif

        $("#moreGuestsBtn").click(function () {
            addNewGuest();
        });

        $('body').on('click', '.guest-delete-btn, .guest-delete-btn-ar', function () {
            var guestID = $(this).attr('id');
            $("#guestCard-" + guestID).remove();
            numberOfGuests--;
        });
        $("#no_visitor_check").click(function () {
            var numberOfGuestCards = $(".guest-card");
            if (numberOfGuestCards.length > 0) {
                    if (isInternetExplorer()) {
                        jQuery(".guest-card").remove();
                        numberOfGuests = 0;
                    } else {
                        jQuery.each(numberOfGuestCards, function (index, cardObject) {
                            cardObject.remove();
                            numberOfGuests--;
                        });
                    }
                $("#moreGuestsBtn").css('display', "none");
            } else {
                addNewGuest();
                $("#moreGuestsBtn").css('display', "block");
            }
        });

        function isInternetExplorer() {
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                return true;
            } else {
                return false
            }

        }

        function addNewGuest() {
            var guestDeleteButton = "<button type='button' id='" + numberOfGuests + "' class='guest-delete-btn'>X</button>";
            if (numberOfGuests == 0) {
                guestDeleteButton = "";
            } else if (currentLocale == "ar") {
                guestDeleteButton = "<button type='button' id='" + numberOfGuests + "' class='guest-delete-btn-ar'>X</button>";
            }
            var htmlContext = '<div class="box guest-card" id="guestCard-' + numberOfGuests + '" style="margin-top:20px; margin-bottom:20px;">' +
                '<div class="panel-heading"><h3 class="panel-title">@lang("validation.attributes.guest")</h3>' + guestDeleteButton + '</div>' +
                '<div class="panel-body">' +
                '<div class="form-group row">' +
                '<div class="col-md-6">' +
                '<input class="form-control" name="visitors[' + numberOfGuests + '][name]" type="text" placeholder="@lang("validation.attributes.name")"required>' +
                '</div>' +
                '<div class="col-md-6">' +
                '<input type="text" name="visitors[' + numberOfGuests + '][mobile]" class="form-control" placeholder="9665xxxxxxx" required>' +
                '</div>' +
                '</div>' +
                '<div class="form-group row">' +
                '<div class="col-md-6">' +
                '<input type="email" name="visitors[' + numberOfGuests + '][email]" class="form-control" placeholder="@lang("validation.attributes.email")" required>' +
                '</div>' +
                '<div class="col-md-6">' +
                '<input type="text" name="visitors[' + numberOfGuests + '][parking]" class="form-control" placeholder="@lang("validation.attributes.parking_number") (@lang("validation.attributes.optional"))">' +
                '</div>' +
                '</div>' +
                '<div class="form-group row">' +
                '<div class="col-md-12">' +
                '<select name="visitors[' + numberOfGuests + '][lang]" class="form-control" required>' +
                '<option value="">@lang("validation.attributes.select_visitor_language")</option>' +
                '<option value="ar">@lang("validation.attributes.ar")</option>' +
                '<option value="en">@lang("validation.attributes.en")</option>' +
                '</select>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';

            $(htmlContext).appendTo("#guestsPanel");
            numberOfGuests++;
        }
    })
</script>