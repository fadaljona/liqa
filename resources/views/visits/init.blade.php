@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @if(session()->has('error'))
                <div class="alert alert-danger">
                    <a href="#" class="close" aria-label="close">&times;</a>
                    {!! session()->get('error') !!}
                </div>
            @endif
            @if($visits->count() > 0)
                @foreach ($visits as $visit)
                    <div class="box">
                        <div class="box-title">
                            <h3>{{$visit->title}}</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <ul class="col-md-12 visit-info">
                                    <li>@lang('validation.attributes.priority_level') :
                                        @lang("validation.attributes.priority.".$visit->priority)
                                    </li>
                                    <li><i class="far fa-calendar-alt"></i>{{ $visit->date->format('Y-m-d') }}</li>
                                    <li>
                                        <i class="far fa-clock"></i>{{  date_format(date_create($visit->time_end),"h:i") }}
                                        - {{  date_format(date_create($visit->time_start),"h:i") }}</li>
                                    <li><i class="fas fa-map-marker"></i>
                                        @if(app()->islocale('en'))
                                            {{$visit->room->name_en}}
                                        @else
                                            {{$visit->room->name_ar}}
                                        @endif
                                    </li>
                                    <li><i class="fas fa-male"
                                           title="@lang('validation.attributes.host')"></i> {{$visit->user->name}}</li>
                                </ul>
                            </div>
                            <div class="col-md-8 col-xs-12">
                                <h4>@lang('validation.attributes.guests')</h4>
                                @if($visit->visitor->count() == 0)
                                    <span>@lang('validation.attributes.no_visitors')</span>
                                    <br/>
                                    <br/>
                                    <br/>
                                    @if(isset($visit->needs))
                                        <h4>@lang('validation.attributes.needs')</h4>
                                        <p>{{ $visit->needs }}</p>
                                    @endif
                                    <br/>
                                    <div class="row mt-10px">
                                        <div class="col-md-2 col-xs-6">
                                            <a class="btn btn-sm pull-right"
                                               href="{{ route('visits.accepted', $visit->id) }}">@lang('validation.attributes.accept')</a>
                                        </div>
                                        <div class="col-md-6 col-xs-6">
                                            <button class="btn btn-sm btn-danger btn-delete pull-left"
                                                    data-toggle="modal"
                                                    data-target="#delete-modal-{{$visit->id}}">@lang('validation.attributes.decline')
                                            </button>
                                            <form method="post" action="{{ route('visits.decline', $visit->id) }}">
                                            {{ csrf_field() }}
                                            <!-- Modal HTML -->
                                                <div id="delete-modal-{{$visit->id}}" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">@lang('messages.decline_confirmation')</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="form-group"><label
                                                                            for="reason">@lang('validation.attributes.decline_reason')</label><textarea
                                                                            name="reason" id="reason" cols="30" rows="6"
                                                                            class="form-control"></textarea></div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">@lang('validation.attributes.no')</button>
                                                                <button type="button" class="btn btn-primary"
                                                                        onclick="$(this).closest('form').submit();">@lang('validation.attributes.yes')</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <br/>
                                @else
                                    @foreach($visit->visitor as $key => $visitor)
                                        <div class="row">
                                            <div class="col-md-6 col-xs-12">
                                                <span class="fa fa-user"></span>
                                                {{$visitor->name}}
                                            </div>
                                            <div class="col-md-6 col-xs-12">
                                                <span class="fa fa-phone"></span>
                                                {{$visitor->mobile}}
                                            </div>
                                            <div class="col-md-6 col-xs-12">
                                                <span class="fa fa-envelope"></span>
                                                {{$visitor->email}}
                                            </div>
                                            <div class="col-md-6 col-xs-12">
                                                <span class="fa fa-car"></span>
                                                {{$visitor->parking ? $visitor->parking : trans('validation.attributes.no_parking')}}
                                            </div>
                                        </div>
                                        <br/>
                                    @endforeach
                                    <div class="row">
                                        @if(isset($visit->needs))
                                            <h4>@lang('validation.attributes.needs')</h4>
                                            <div class="col-md-12 col-xs-12">
                                                <p>{{ $visit->needs }}</p>
                                            </div>
                                        @endif
                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-2 col-xs-6">
                                            <a class="btn btn-sm pull-right"
                                               href="{{ route('visits.accepted', $visit->id) }}">@lang('validation.attributes.accept')</a>
                                        </div>
                                        <div class="col-md-6 col-xs-6">
                                            <button class="btn btn-sm btn-danger btn-delete pull-left"
                                                    data-toggle="modal"
                                                    data-target="#delete-modal-{{$visit->id}}">@lang('validation.attributes.decline')
                                            </button>
                                            <form method="post" action="{{ route('visits.decline', $visit->id) }}">
                                            {{ csrf_field() }}
                                            <!-- Modal HTML -->
                                                <div id="delete-modal-{{$visit->id}}" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">@lang('messages.decline_confirmation')</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <label for="reason">@lang('validation.attributes.cancellation_reason') - @lang('validation.attributes.optional')</label>
                                                                    <textarea name="reason" id="reason" class="form-control" rows="6"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">@lang('validation.attributes.no')</button>
                                                                <button type="button" class="btn btn-primary"
                                                                        onclick="$(this).closest('form').submit();">@lang('validation.attributes.yes')</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <br/>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="text-center">
                    {{ $visits->appends(Request::except('page'))->links() }}
                </div>
            @else
                <div class="box">
                    <div class="no-data">@lang('validation.attributes.no_data')</div>
                </div>
            @endif
        </div>
    </div>
@endsection
