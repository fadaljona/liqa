@extends('layouts.app')

@section('content')
<div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">@lang('validation.attributes.login')</h3>
                    @if(App::isLocale('en'))
                        <a href="{{ route('lang', [ 'ar']) }}" style="float:left;">العربية</a>
                    @else
                        <a href="{{ route('lang', [ 'en']) }}" style="float:left;">English</a>
                    @endif
                </div>
                <div class="panel-body">
                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            <a href="#" class="close" aria-label="close">&times;</a>
                            {!! session()->get('error') !!}
                        </div>
                    @endif
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">@lang('validation.attributes.email')</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">@lang('validation.attributes.password')</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label style="padding-right: 22px;
    padding-left: 10px;">
                                        <input style="margin-left: 0;
    margin-right: -22px;" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> <span class="text">@lang('validation.attributes.remember_me')</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    @lang('validation.attributes.login')
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    @lang('validation.attributes.i_forgot_my_password')
                                </a>
                            </div>
                        </div>
                        <hr/>
                        <a role="button" class="btn btn-default btn-block" href="{{ $link }}"
                           data-provider="office365" data-link_type="login"> <span class="default">Office 365</span>
                            <img src="https://a.edim.co/images_v2/icons/icon_office365_50.png" width="25"  />
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
