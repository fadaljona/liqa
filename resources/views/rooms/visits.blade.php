<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=0">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-rtl.min.css') }}">
    <link href="https://unpkg.com/ionicons@4.2.2/dist/css/ionicons.min.css" rel="stylesheet">
    <link href="{{ asset('css/room_visit_style.css') }}" rel="stylesheet">
</head>
<body>
<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 col-xs-2">
                <a class="navbar-brand" href="#">
                    @if(isset($settings->logo))
                        <img class="navbar-logo" src="{{ asset('/images/'.$settings->logo)}}"/>
                    @else
                        <img class="navbar-logo" src="{{ asset('/images/logo.png')}}"/>
                    @endif
                </a>
            </div>
            <div class="col-md-8 col-xs-8">
                <h1>
                    @if(app()->getLocale() == "ar")
                        {{$room->name_ar}}
                    @else
                        {{$room->name_en}}
                    @endif
                </h1>
            </div>
            <div class="col-md-2 col-xs-2">
                <a class="navbar-brand" href="#">
                    @if(isset($settings->logo))
                        <img class="navbar-logo" src="{{ asset('/images/'.$settings->logo)}}"/>
                    @else
                        <img class="navbar-logo" src="{{ asset('/images/logo.png')}}"/>
                    @endif
                </a>
            </div>
        </div>
    </div>
</div><!-- .header -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-7 col-sm-6">
            <table class="table" id="time-table">
                <thead>
                <tr>
                    <th><img id="meetings-logo" src="{{ asset('images/meetings_icon.png') }}">|
                        <span>الاجتماع</span>|
                        <span>Meeting</span>
                    </th>
                    <th>الوقت <span style="margin-right:10px;">|</span></th>
                    <th>Time</th>
                </tr>
                </thead>
                <tbody>
                @foreach($room->visits as $visit)
                    <tr>
                        <td class="col-md-3">{{$visit->title}}</td>
                        <td class="col-md-2">{{ date('g:i A', strtotime($visit->time_start)) }} <i class="icon ion-md-arrow-back" style="margin-right: 10px;"></i></td>
                        <td class="col-md-1">{{ date('g:i A', strtotime($visit->time_end)) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
         <div class="col-md-5 col-sm-6">
                <div class="current-meeting-label">
                    @if($currentMeeting != null)
                    <p id="now-paragraph"><span>الان</span><span>NOW</span></p>
                    <h1>{{$currentMeeting->title}}</h1>
                    <p id="start-finish-time-paragraph"><span>{{ date('g:i A', strtotime($currentMeeting->time_start)) }}</span><i class="icon ion-md-arrow-back"></i><span>{{ date('g:i A', strtotime($currentMeeting->time_end)) }}</span></p>
                    @else
                        <p>يمكنك حجز قاعة الاجتماعات هـذه عن طريق زيارة الرابط</p>
                        <p>You can book this meeting room by visiting the following link</p>
                        <h3>{{ env('APP_URL') }}</h3>
                    @endif
                </div>
            </div>
    </div>
</div>
<div class="current-date-label">
    Date | {{Date('Y-m-d')}}
</div>
<footer>
    <p>@lang('messages.screen_support')&nbsp;@if(isset($setting->support_mail)) {{ $setting->support_mail }} @endif</p>
</footer>
<script src="{{ asset('js/app.js') }}"></script>
<script>
    $(document).ready(function(){
        setInterval(function(){
            if(navigator.onLine){
                location.reload();
            }
        }, 10000);
    });
</script>
</body>
</html>
