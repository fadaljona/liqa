@extends('layouts.app')

@section('content')
    <div class="row page-title">
        <div class="col-md-11 col-xs-8"><h3 class="col-md-6">@lang('validation.attributes.meeting_rooms')</h3></div>
        <div class="col-md-1 col-xs-4"><a href="{{route('rooms.create')}}" class="visitors-theme-button pull-left" id="newRoomBtn">@lang('validation.attributes.add')</a></div>

    </div>
    <div class="box" style="overflow: auto;">
        @if(count($rooms) === 0)
            <div class="no-data">@lang('validation.attributes.no_data')</div>
        @else
            <table class="table">
                <thead>
                <th>@lang('validation.attributes.name_ar')</th>
                <th>@lang('validation.attributes.name_en')</th>
                <th>@lang('validation.attributes.size')</th>
                <th>@lang('validation.attributes.request_approval')</th>
                <th></th>
                </thead>
                <tbody>
                @foreach($rooms as $room)

                    <tr>
                        <td>{{$room->name_ar}}</td>
                        <td>{{$room->name_en}}</td>
                        <td>{{$room->size}}</td>
                        <td>
                            @if($room->approval == 0)
                                @lang('validation.attributes.no')
                                @else
                            @lang('validation.attributes.yes')
                                @endif
                        </td>
                        <td class="text-left">
                            <a class="btn btn-sm" href="{{ route('rooms.edit', $room->id) }}">@lang('validation.attributes.edit')</a>
                            <a class="btn btn-sm" href="{{ route('rooms.visit', $room->id) }}">@lang('validation.attributes.link')</a>
                            <button class="btn btn-sm btn-danger btn-delete" data-toggle="modal"
                                    data-target="#delete-modal-{{$room->id}}">@lang('validation.attributes.delete')</button>
                            <form method="post" action="{{ route('rooms.destroy', $room->id) }}">
                                <input type="hidden" name="_method" value="delete"/>
                                {{ csrf_field() }}
                                <!-- Modal HTML -->
                                <div id="delete-modal-{{$room->id}}" class="modal fade">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">@lang('messages.delete_confirmation')</h4>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default"
                                                        data-dismiss="modal">@lang('validation.attributes.no')</button>
                                                <button type="button" class="btn btn-primary"
                                                        onclick="$(this).closest('form').submit();">@lang('validation.attributes.yes')</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
    </div>
@endsection
