@extends('layouts.app')

@section('content')
    <div class="row page-title">
        <div class="col-md-12">
            <h3 class="col-md-6">@lang('validation.attributes.meeting_rooms')</h3>
        </div>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div class="box">
            <div class="panel-body">
                <form method="post" action="{{ route('rooms.store') }}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.name_ar')</label>
                        <div class="col-md-8"><input value="{{ old('name_ar') }}" type="text" name="name_ar"
                                                     class="form-control" required autofocus/></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.name_en')</label>
                        <div class="col-md-8"><input value="{{ old('name_en') }}" type="text" name="name_en"
                                                     class="form-control" required autofocus/></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.size')</label>
                        <div class="col-md-8"><input value="{{ old('size') }}" type="number" name="size"
                                                     class="form-control" required autofocus/></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.request_approval')</label>
                        <div class="col-md-8">
                            <select name="approval" class="form-control" required autofocus>
                                <option value="0" @if(old('approval') == 0) selected @endif>@lang('validation.attributes.approval_not_required')</option>
                                <option value="1" @if(old('approval') == 1) selected @endif>@lang('validation.attributes.approval_required')</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group"><label for="location" class="col-md-4">@lang('validation.attributes.location')</label>
                        <div class="col-md-8">
                            <input value="{{ old('location') }}" id="location" type="text" name="location"
                                   class="form-control" placeholder="https://goo.gl/maps/xxxxx" autofocus/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group"><label for="floor" class="col-md-4">@lang('validation.attributes.floor')</label>
                        <div class="col-md-8">
                            <select name="floor" id="floor" class="form-control" required autofocus>
                                <option value="">@lang('messages.please_choose')</option>
                                @for($i = 1; $i <= 20; $i++)
                                <option value="{{$i}}" @if(old('floor') == $i) selected @endif>@lang('validation.attributes.floor') {{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group"><label for="" class="col-md-4">@lang('validation.attributes.direction')</label>
                        <div class="col-md-8">
                            <select name="direction" id="" class="form-control" required autofocus>
                                <option value="left" @if(old('direction') == "left") selected @endif>@lang('validation.attributes.left')</option>
                                <option value="right" @if(old('direction') == "right") selected @endif>@lang('validation.attributes.right')</option>
                            </select></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="@lang('validation.attributes.save')"
                               class="btn visitors-theme-button saveBtn pull-left"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection