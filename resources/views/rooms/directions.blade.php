<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Directions Screen</title>

    <!-- Bootstrap -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-rtl.min.css') }}">
    <link rel="stylesheet" href="{{ asset('fontawesome/css/font-awesome.css') }}">
    <link href="{{ asset('css/direction_screen.css') }}" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            <a class="navbar-brand" href="#">
                @if(isset($settings->logo))
                    <img class="navbar-logo" src="{{ asset('/images/'.$settings->logo)}}"/>
                @else
                    <img class="navbar-logo" src="{{ asset('/images/logo.png')}}"/>
                @endif
            </a>
        </div>
        <div class="col-md-8" id="header-info">
            <h1 class="text-center floor_name">الدور {{$location}}<small><br><p id="currentTime"></p></small></h1>
        </div>
        <div class="col-md-2">
            <a class="navbar-brand" href="#">
                @if(isset($settings->logo))
                    <img class="navbar-logo" src="{{ asset('/images/'.$settings->logo)}}"/>
                @else
                    <img class="navbar-logo" src="{{ asset('/images/logo.png')}}"/>
                @endif
            </a>
        </div>
    </div>
    <div class="vertical-space"></div>
    @foreach($rooms as $room)
        <div class="panel panel-default">
            <div class="panel-body" id="room_name_container">
                <table width="100%" border="0">
                    <tbody><tr>
                        <td align="center">
                            <h1 class="room_name">
                                @if(app()->getLocale() == "ar")
                                    {{$room->name_ar}}
                                @else
                                    {{$room->name_en}}
                                @endif
                                @if($room->current_meeting() != null)
                                    <br>
                                        <small>
                                            {{$room->current_meeting()->title}}
                                        </small>
                                @endif
                            </h1>
                        </td>
                        <td width="140" valign="middle">
                            <i class="fa fa-long-arrow-{{ $room->direction }} arrow-icon"></i></td>
                    </tr>
                    </tbody></table>
            </div>
        </div>
    @endforeach
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script>
    setInterval(function(){
        var date = new Date();
        $("#currentTime").html(
            date.getHours() + ":" + (date.getMinutes()<10?'0':'') + date.getMinutes() + ":" + (date.getSeconds()<10?'0':'') + date.getSeconds()
        );
    }, 1000)
    setInterval(function(){
        if(navigator.onLine){
            window.location.reload(true);
        }
    }, 60000)
</script>
</body>
</html>
