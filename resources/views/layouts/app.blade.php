<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="locale" content="{{ app()->getLocale() }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@lang('validation.attributes.meetings_management_system')</title>

    <link rel="mask-icon" href="/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- Scripts -->
    <script src="{{ asset('/js/app.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="//www.fontstatic.com/f=cairo-bold,cairo"/>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/visitors-theme.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('css/easy-autocomplete.min.css') }}"/>

    @if(App()->isLocale('ar'))
        <link rel="stylesheet" href="//cdn.rawgit.com/morteza/bootstrap-rtl/v3.3.4/dist/css/bootstrap-rtl.min.css">
    @endif


    <script>
        $(document).on('click', '.alert .close', function () {
            $(this).closest('.alert')
                .animate({
                    height: 'toggle', opacity: 'toggle', padding: 'toggle', margin: 'toggle'
                }, 500);
            return false;
        });
    </script>


    <link rel="manifest" href="{{ asset('/manifest.json') }}"> <!-- for https -->
    @yield('meta')
</head>
<body>
<div id="app">
    @if (Auth()->guest())
        @if(isset($settings->logo))
        <div id="brand"><img src="{{ asset('/images/'.$settings->logo)  }}"/></div>
        @else
        <div id="brand"><img src="{{ asset('/images/logo_ white.png')  }}"/></div>
        @endif
    @else
        @include('layouts.nav')
    @endif

    <div class="container">

        @if(session()->has('success'))
            <div class="alert alert-success">
                <a href="#" class="close" aria-label="close">&times;</a>
                <i class="fa fa-check"></i> {!! session()->get('success') !!}
            </div>
        @endif
            @if(session()->has('danger'))
                <div class="alert alert-danger">
                    <a href="#" class="close" aria-label="close">&times;</a>
                    {!! session()->get('danger') !!}
                </div>
            @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @yield('content')
    </div>
</div>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="{{asset('js/functions.js')}}"></script>
@yield('js')
</body>
</html>
