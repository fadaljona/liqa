<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{route('home')}}">
                @if(isset($settings->logo))
                    <img class="navbar-logo" src="{{ asset('/images/'.$settings->logo)}}"/>
                @else
                    <img class="navbar-logo" src="{{ asset('/images/logo.png')}}"/>
                @endif
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                @if(auth()->user()->canDo('visits.index'))
                    <li><a href="{{ route('visits.index') }}">@lang('validation.attributes.visits')</a></li>
                @endif
                    @if(auth()->user()->canDo('rooms.index'))
                        <li><a href="{{ route('rooms.index') }}">@lang('validation.attributes.meeting_rooms')</a></li>
                    @endif
                    @if(auth()->user()->canDo('visits.init'))
                        <li><a href="{{ route('visits.init') }}">@lang('validation.attributes.visit_requests')</a></li>
                    @endif
                    @if(auth()->user()->canDo('users.index'))
                        <li><a href="{{ route('users.index') }}">@lang('validation.attributes.users')</a></li>
                    @endif
                    @if(auth()->user()->canDo('reports.index'))
                        <li><a href="{{ route('reports.index') }}">@lang('validation.attributes.reports')</a></li>
                    @endif
                    @if(auth()->user()->canDo('setting.edit'))
                        <li><a href="{{ route('setting.edit') }}">@lang('validation.attributes.setting')</a></li>
                    @endif
            </ul>
            <ul class="nav navbar-nav @if(app()->isLocale('en')) navbar-right @else navbar-left @endif">
                @if(app()->isLocale('en'))
                    <li><a href="/lang/ar">العربية</a></li>
                @else
                    <li><a href="/lang/en">ENGLISH</a></li>
                @endif
                @auth
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{auth()->user()->name}} <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                @if(auth()->user()->canDo('logout'))
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        @lang('validation.attributes.logout')
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                                @endif
                            </ul>
                        </li>
                @else
                    <li><a href="{{route('login')}}">@lang('validation.attributes.login')</a></li>
                    <li><a href="{{route('register')}}">@lang('validation.attributes.register')</a></li>
                @endauth
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
