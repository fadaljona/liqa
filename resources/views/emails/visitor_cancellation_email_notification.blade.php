<p style="@if($notifiable->lang == "en") direction: rtl; float: right;@endif line-height: 2;">
    @if($notifiable->lang == "en")
        Dear {{$notifiable->name}},<br>
        Your meeting titled {{$meeting->title}} with {{$meeting->user->name}}
        <br>
        has been cancelled
        <br>
        Best regards,<br>
        {{ $company_name_en }}
    @else
        عزيزي {{$notifiable->name}}،<br>
        لقد تم الغاء اجتماعكم بعنوان {{$meeting->title}} مع {{$meeting->user->name}}
        <br>
        مع اطيب التحيات
        <br>
        {{ $company_name_ar }}
    @endif
</p>