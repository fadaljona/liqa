<p style="@if($notifiable->lang == "ar") direction: rtl; float: right;@endif line-height: 2;">
                @if($notifiable->language == "en")
                    Dear {{$notifiable->name}},<br>
                    Your meeting titled {{$meeting->title}} has been cancelled successfully. If there are any guests, they will be notified immediately.
                    <br>
                    Best regards,<br>
                    {{ $company_name_en }}
                @else
                    عزيزي {{$notifiable->name}}،<br>
                    لقد تم الغاء اجتماعكم بعنوان {{$meeting->title}} بنجاح. اذا كان هناك اي ضيوف من خارج الشركة، سيتم اعلامهم فورا
                    <br>
                    مع اطيب التحيات
                    <br>
                    {{ $company_name_ar }}
                @endif
</p>