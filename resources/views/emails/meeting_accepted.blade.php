<p style="@if($notifiable->lang == "ar") direction: rtl; float: right;@endif line-height: 2;">
    @if($notifiable->language == "ar")
    <b>عزيزي  {{$notifiable->name}}</b>,
                    <br>
                    لقد تم قبول اجتماعكم بعنوان {{$meeting->title}}
                    <br><br><br>مع اطيب التحيات,<br>{{$company_name_ar}}
            @else
                <b>Dear {{$notifiable->name}}</b>,
                    <br>
                    Your meeting titled {{ $meeting->title }} has been accepted

                    <br><br><br>Best regards,<br>{{ $company_name_en }}
            @endif
</p>
