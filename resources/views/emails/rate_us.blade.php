<p style="@if($notifiable->lang == "ar") direction: rtl; float: right;@endif line-height: 2;">
    @if($notifiable->language == "ar")
                شكرا لك  لزيرتنا ويسرنا تقيمك لتحسين الأداء
                عبر الرابط التالي :
                <a href="{{$visit_rates_link}}">اضفط هنا</a>
    @else
        Thank you for visiting us and we are pleased to rate your visit to our company via the following link:
        <a href="{{$visit_rates_link}}">Here</a>
    @endif
</p>
