<p style="@if($meeting->user->language == "ar") direction: rtl; float: right; @endif line-height: 2;">
    @if($meeting->user->language == "ar")
        عزيزي <b>{{$meeting->user->name}}</b>،
        <br>
        @if(isset($reason))
            لقد تم رفض اجتماعكم بعنوان {{$meeting->title}} وذلك للاسباب التالية:
            <br>
            <blockqoute>{{ $reason }}</blockqoute>
        @else
            لقد تم رفض اجتماعكم بعنوان {{$meeting->title}}.
        @endif
        <br>
        <br>
        <br>
        مع اطيب التحيات،
        <br>
        {{$company_name_ar}}
    @else
        Dear <b>{{$meeting->user->name}}</b>,
        <br>
        @if(isset($reason))
            Your meeting titled {{$meeting->title}} has been declined for the following reason:
            <br>
            <blockquote>{{$reason}}</blockquote>
        @else
                Your meeting titled {{$meeting->title}} has been declined.
        @endif
            <br>
            <br>
            <br>
            Best regards,
            <br>
            {{$company_name_en}}
    @endif
</p>