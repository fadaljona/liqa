<p style="@if($notifiable->lang == "ar") direction: rtl; float: right;@endif line-height: 2;">
    @if($notifiable->lang == "en")
        Dear {{$notifiable->name}},<br>
        Here are your visit details to {{$company_name_en}} with {{$meeting->user->name}}
        <br>
        Your visit pass: <a href="{{$visit_details_link}}">Here</a>
        <br>
        looking forward for your visit.


        best regards,
        {{$company_name_en}}
    @else
        عزيزي {{$notifiable->name}}،<br>
        هذه تفاصيل زيارتك لـ {{$company_name_ar}} مع {{$meeting->user->name}}
        <br>
        تصريح الزيارة الخاص بك: <a href="{{$visit_details_link}}">هنا</a>
        <br>
        نتطلع لزيارتك

        مع تحياتنا،
        <br>{{$company_name_ar}}
    @endif
</p>