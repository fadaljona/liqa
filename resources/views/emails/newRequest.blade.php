<p style="text-align:right">@lang('messages.welcome')</p>

<p style="text-align:right">&nbsp;</p>

<p style="text-align:right">@lang('messages.email_new_request')
<a href="{{ route('visits.init') }}">@lang('messages.view_request')</a></p>

<p style="text-align:right">&nbsp;</p>

<p style="text-align:right">&nbsp;</p>

<p style="text-align:right">@lang('validation.attributes.reservation_management_system')</p>

<p style="text-align:right">@lang('validation.attributes.it_department')</p>
