@extends('layouts.app')

@section('content')
    <div class="row page-title">
        <div class="col-md-12">
            <h3 class="col-md-6">@lang('validation.attributes.setting')</h3>
        </div>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div class="box">
            <div class="panel-body">
                <form method="post" action="{{ route('setting.update') }}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="put"/>
                    <input type="hidden" name="id" value="{{$setting->id}}"/>

                    <h4>@lang('validation.attributes.general')</h4>
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.company_ar_name')</label>
                        <div class="col-md-8"><input value="{{old('company_name',$setting->company_name)}}" type="text"
                                                     name="company_name"
                                                     class="form-control" required autofocus/></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.company_en_name')</label>
                        <div class="col-md-8">
                            <input value="{{old('company_name_en',$setting->company_name_en)}}" type="text"
                                   name="company_name_en"
                                   class="form-control" required autofocus/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.company_domain')
                            ( @lang('validation.attributes.company_domain_note') )</label>
                        <div class="col-md-8">
                            <input value="{{old('company_domain',$setting->company_domain)}}" type="text"
                                   placeholder="google.com"
                                   name="company_domain" class="form-control"/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.support_mail')</label>
                        <div class="col-md-8"><input value="{{ old('support_mail', $setting->support_mail) }}"
                                                     type="email"
                                                     name="support_mail" class="form-control" required autofocus/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4">@lang('validation.attributes.logo')</label>
                        @if($setting->logo)
                            <img class="min-logo" src="{{asset('images/'.old('logo', $setting->logo)) }}" alt="">
                            <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#myModal">@lang('validation.attributes.delete')</button>
                        @else
                            <div class="col-md-8">
                                <input type="file" name="logo" class="form-control" autofocus value="{{$setting->logo}}"/>
                            </div>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="@lang('validation.attributes.save')"
                               class="btn visitors-theme-button saveBtn pull-left"/>
                    </div>
                </form>
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                        <form action="{{route('setting.logo.delete',$setting->id)}}" method="get" >
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <h2>@lang('validation.attributes.modal_question')</h2>
                                </div>
                                <input type="text" class="hidden" name="id" value="{{$setting->id}}">
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">@lang('validation.attributes.close')</button>
                                    <input type="submit" class="btn btn-danger" value="@lang('validation.attributes.delete')">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection