@extends('layouts.app')


@section('content')
    <div class="row page-title">
        <div class="col-md-12">
            <h3 class="col-md-6">@lang('validation.attributes.reports')</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="box">
                <h1 class="text-center">{{ $meetings_count }}</h1>
                <div class="panel-body text-center">
                    <p>@lang('strings.total_meetings')</p>
                    <a href="{{route('reports.meetings.export')}}">{{ trans('strings.download_all_data') }}</a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="box">
                <h1 class="text-center">{{ $visitors_count }}</h1>
                <div class="panel-body text-center">
                    <p>@lang('strings.total_visitors')</p>
                    <a href="{{route('reports.visitors.export')}}">{{ trans('strings.download_all_data') }}</a>
                    <br/>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="box">
                <h1 class="text-center">{{ $users_count  }}</h1>
                <div class="panel-body text-center">
                    <p>@lang('strings.total_members')</p>
                    <a href="{{route('reports.users.export')}}">{{ trans('strings.download_all_data') }}</a>
                    <br/>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="box">
                <h1 class="text-center">{{$average_duration}}</h1>
                <div class="panel-body text-center">
                    <p>@lang('strings.average_duration_of_meetings')<br/>@lang('strings.per_minute')</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <h4 class="text-center">@lang('strings.top_5_users_use_system')</h4>
                <div class="panel-body">
                    <table class="table" id="usersListTable">
                        <thead>
                        <tr>
                            <th style="text-align: center">@lang('validation.attributes.name')</th>
                            <th style="text-align: center">@lang('strings.number_of_reservations')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td style="text-align: center">{{$user->name}}</td>
                                <td style="text-align: center">{{$user->number_of_visits}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box">
                <h4 class="text-center">@lang('strings.top_5_meetings_used')</h4>
                <div class="panel-body">
                    <table class="table" id="usersListTable">
                        <thead>
                        <tr>
                            <th style="text-align: center">@lang('validation.attributes.name')</th>
                            <th style="text-align: center">@lang('strings.number_of_reservations')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($rooms as $room)
                            <tr>
                                <td style="text-align: center">{{$room->name_ar}} - {{$room->name_en}}</td>
                                <td style="text-align: center">{{$room->number_of_visits}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <h4 class="text-center">@lang('strings.last_5_reviews')</h4>
                <div class="panel-body text-center">
                    <table class="table">
                        <thead>
                        <tr>
                            <th style="text-align: center">@lang('validation.attributes.visit_title')</th>
                            <th style="text-align: center">@lang('validation.attributes.cleanness')</th>
                            <th style="text-align: center">@lang('validation.attributes.host')</th>
                            <th style="text-align: center">@lang('validation.attributes.service')</th>
                            <th style="text-align: center">@lang('validation.attributes.text')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($rates as $rate)
                            <tr>
                                <td style="text-align: center">{{$rate->visit->title}}</td>
                                <td style="text-align: center">{{$rate->cleanness}} / 5</td>
                                <td style="text-align: center">{{$rate->host}} / 5</td>
                                <td style="text-align: center">{{$rate->service}} / 5</td>
                                <td style="text-align: center">{{$rate->text}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <a href="{{route('reports.rates.export')}}">{{ trans('strings.download_all_data') }}</a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box">
                <h4 class="text-center">@lang('strings.total_meetings_per_months')</h4>
                <div class="panel-body">
                    {!! $count_meetings_by_month->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('js/chart.js') }}"></script>
@endsection

