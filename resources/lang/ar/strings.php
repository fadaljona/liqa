<?php

return [
	"total_meetings" => "إجمالي الاجتماعات",
	"total_visitors" => "إجمالي الزوار",
	"total_members" => "إجمالي الأعضاء",
	"per_minute" => "(بالدقيقة)",
	"average_duration_of_meetings" => "متوسط مدة الإجتماعات",
	"top_5_users_use_system" => "أكثر 5 مستخدمين استخدمآ للنظام",
	"number_of_reservations" => "عدد الحجزات",
	"top_5_meetings_used" => "أكثر 5 غرف الإجتماعات حجزآ",
	"total_meetings_per_months" => "اجمالي الإجتماعات حسب الشهر",
	'january' => 'يناير',
	'february'=> 'فبراير',
	'march' => 'مارس',
	'april'=> 'ابريل',
	'may'=> 'مايو',
	'june'=> 'يونيو',
	'july'=> 'يوليو',
	'august'=> 'أغسطس',
	'september'=> 'سبتمبر',
	'october'=> 'أكتوبر',
	'november'=> 'نوفمبر',
	'december'=> 'ديسمبر',
    'last_5_reviews' => 'آخـر 5 تقييمات',
    'download_all_data' => 'تحميل جميع البيانات'
];
