<?php

return [
	"total_meetings" => "Total meetings",
	"total_visitors" => "Total visitors",
	"total_members" => "Total members",
	"per_minute" => "(Per minute)",
	"average_duration_of_meetings" => "Average duration of meetings",
	"top_5_users_use_system" => "Top 5 users have used the system",
	"number_of_reservations" => "Number of reservations",
	"top_5_meetings_used" => "Top 5 meeting rooms reserved",
	"total_meetings_per_months" => "Total meetings by month",
	'january' => 'January',
	'february'=> 'February',
	'march' => 'March',
	'april'=> 'April',
	'may'=> 'May',
	'june'=> 'June',
	'july'=> 'July',
	'august'=> 'August',
	'september'=> 'September',
	'october'=> 'October',
	'november'=> 'November',
	'december'=> 'December',
    'last_5_reviews' => 'Last 5 reviews'
];
