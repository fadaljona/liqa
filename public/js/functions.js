$(document).ready(function(){
    var currentLocale = $("meta[name=locale]").attr("content");
    //things to do if current locale is arabic
    if(currentLocale == "ar"){
        $("#main-visit-btn").addClass("pull-left");
        $("#newDeptartmentForm").addClass("pull-left");
        $("#newUserBtn").addClass("pull-left");
        $("#newRoomBtn").addClass("pull-left").removeClass("pull-right");
        $(".guest-delete-btn").addClass('pull-left');
        $("#guest_checkin_btn").removeClass('pull-right').addClass('pull-left');
    }else{
        //things to do if the locale is English
        $("#main-visit-btn").addClass("pull-right");
        //move the visit-sorting div to the far left
        $(".visits-sorting span").css({"float": "left",
        "display": "block",
        "clear": "both"});
        $(".visits-sorting ul").css({
            "left": "-53px",
        "padding": "0",
        "top": "25px"
        });
        $("#newDeptartmentForm").addClass("pull-right");
        $("#newUserBtn").addClass("pull-right");
        $("#newRoomBtn").addClass("pull-right").removeClass("pull-left");
        $(".text-left").css("padding-left","135px");
        $(".panel-heading a").css("float","right");
        $(".checkbox label .text").css('margin-left',"20px");
        $("#guest_checkin_btn").css("left","138px").css("top","-20px");
        $("#guest_checkout_btn").css("left","235px").css("top","-20px");
    }
    $("#usersListTable_filter input[type=search]").addClass("filter-form-control");
});